def hash(s: String): Int = {
    def _hash(acc: Int, remaining: String): Int = {
        if (remaining.isEmpty) acc
        else {
            val v1 = (acc + remaining.head.toInt) % 256
            val v2 = (v1 * 17) % 256
            _hash(v2, remaining.tail)
        }
    }
    _hash(0, s)
}

val input: String = io.Source.stdin.getLines().next()
val fragments = input.split(",")
val result = fragments.map(hash).sum
println(result)