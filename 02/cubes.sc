case class BagContents(val red: Int, val green: Int, val blue: Int) {
    override def toString(): String = {
        s"(${red} R, ${green} G, ${blue} B)"
    }
}
sealed trait CubeColor
case object Red extends CubeColor
case object Green extends CubeColor
case object Blue extends CubeColor

def parseColor(s: String): CubeColor = {
    s match {
        case "red" => Red
        case "green" => Green
        case "blue" => Blue
        case _ => throw new RuntimeException(s"Invalid color name '${s}'")
    }
}

case class Game(val index: Int, extractions: Array[BagContents]) {
    override def toString(): String = {
        s"${index}: ${extractions.mkString(", ")}"
    }
}

val contents = new BagContents(12, 13, 14)

def parseLine(line: String): Game = {
    assert(line.startsWith("Game "))
    val gameIndex = line.substring(5).takeWhile(_ != ':').toInt

    def parseGame(game: String): BagContents = {
        def parseElement(e: String): (CubeColor, Int) = {
            val p = e.split(" ")
            (parseColor(p(1)), p(0).toInt)
        }

        def combineElements(es: Array[(CubeColor, Int)]): BagContents = {
            var r = 0
            var g = 0
            var b = 0

            for (e <- es) {
                e._1 match {
                    case Red => r = e._2
                    case Green => g = e._2
                    case Blue => b = e._2
                }
            }

            new BagContents(r, g, b)
        }

        combineElements(game.split(", ").map(parseElement))
    }

    val games = line.dropWhile(_ != ':').substring(2).split("; ").map(parseGame)

    new Game(gameIndex, games)
}

def isGamePossible(constraint: BagContents, game: Game): Boolean = {
    game.extractions.map(extraction => {
        extraction.red <= constraint.red &&
        extraction.green <= constraint.green &&
        extraction.blue <= constraint.blue
    }).forall(identity)
}

println(
    io.Source.stdin.getLines().map(parseLine)
        .filter(game => isGamePossible(contents, game))
        .map(_.index)
        .sum
)