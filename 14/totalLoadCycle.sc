import scala.collection.mutable.TreeSet

sealed trait Space
case object Empty extends Space
case object StillRock extends Space
case object RollingRock extends Space

case class Rock(row: Int, col: Int)


def boardRepresentation(nRows: Int, nCols: Int, rollingRocksInRow: Array[List[Rock]], stillRocks: List[Rock]): String = {
    val rows = (for {
        _ <- 0 until nRows
    } yield {
        (for {
            _ <- 0 until nCols
        } yield ".").toArray
    }).toArray

    for {
        row <- rollingRocksInRow
        rollingRock <- row
    } {
        rows(rollingRock.row)(rollingRock.col) = "O"
    }

    for {stillRock <- stillRocks} {
        rows(stillRock.row)(stillRock.col) = "#"
    }

    rows.map(row => row.mkString("")).mkString("\n")
}

class Board(rows: Array[Array[Space]]) {
    require(rows.forall(row => row.length == rows(0).length))

    def precomputeTilt(col: Array[Space]): Array[Int] = {
        def _colNorthTilt(acc: List[Int], lastStillRow: Int, rowIdx: Int): Array[Int] = {
            if (rowIdx >= col.length) acc.reverse.toArray
            else col(rowIdx) match {
                case Empty => _colNorthTilt(lastStillRow :: acc, lastStillRow, rowIdx + 1)
                case StillRock => _colNorthTilt(lastStillRow :: acc, rowIdx, rowIdx + 1)
                case RollingRock => _colNorthTilt(lastStillRow :: acc, lastStillRow, rowIdx + 1)
            }
        }
        _colNorthTilt(Nil, -1, 0) ensuring(_.length == col.length)
    }

    val cols = rows.transpose
    val precomputedNorthTilt: Array[Array[Int]] = cols.map(precomputeTilt).toArray
    val precomputedSouthTilt: Array[Array[Int]] = cols.map(col=> precomputeTilt(col.reverse).map(col.length - _ - 1).reverse).toArray
    val precomputedWestTilt: Array[Array[Int]] = rows.map(precomputeTilt).toArray
    val precomputedEastTilt: Array[Array[Int]] = rows.map(row => precomputeTilt(row.reverse).map(row.length - _ - 1).reverse).toArray

    val nRows = rows.length
    val nCols = cols.length

    val (rocksInRow, rocksInCol): (Array[List[Rock]], Array[List[Rock]]) = {
        val _rocksInRow: Array[List[Rock]] = (0 until rows.length).map(_ => Nil).toArray
        val _rocksInCol: Array[List[Rock]] = (0 until cols.length).map(_ => Nil).toArray

        for {
            i <- 0 until nRows
            j <- 0 until nCols
        } {
            if (rows(i)(j) == RollingRock) {
                val rock = new Rock(i, j)
                _rocksInRow(i) = rock :: _rocksInRow(i)
                _rocksInCol(j) = rock :: _rocksInCol(j)
            }
        }
        
        (_rocksInRow, _rocksInCol)
    }

    val allStillRocks: List[Rock] = 
        (for {
            i <- 0 until nRows
            j <- 0 until nCols
        } yield {
            if (rows(i)(j) == StillRock) Some(new Rock(i, j))
            else None
        }).flatten.toList

    def singleCycle: Unit = {
        def tiltCols(direction: Int, precomputedTilts: Array[Array[Int]]): Unit = {
            (0 until nRows).foreach(rocksInRow.update(_, Nil))

            def tiltCol(reversedCol: List[Rock], precomputedTilt: Array[Int]): List[Rock] = {
                val nilRockList: List[Rock] = Nil
                val col = reversedCol.reverse

                col.foldLeft((nilRockList, -1, 0))((acc, rock) => {
                    val (newCol, nextStillRock, nPiledUp) = acc
                    val oldRow = rock.row

                    val precedingStillRock = precomputedTilt(oldRow)
                    val newRow =
                        if (precedingStillRock == nextStillRock) precedingStillRock + (nPiledUp + 1) * direction
                        else precedingStillRock + direction
                    val newPiledUp =
                        if (precedingStillRock == nextStillRock) nPiledUp + 1
                        else 1

                    val newRock = new Rock(newRow, rock.col)

                    rocksInRow(newRow) = newRock :: rocksInRow(newRow)

                    (newRock :: newCol, precedingStillRock, newPiledUp)
                })._1
            }

            for {j <- 0 until nCols} {
                rocksInCol(j) = tiltCol(rocksInCol(j), precomputedTilts(j))
            }
        }

        def tiltRows(direction: Int, precomputedTilts: Array[Array[Int]]): Unit = {
            (0 until nCols).foreach(rocksInCol.update(_, Nil))

            def tiltRow(reversedRow: List[Rock], precomputedTilt: Array[Int]): List[Rock] = {
                val nilRockList: List[Rock] = Nil
                val row = reversedRow.reverse

                row.foldLeft((nilRockList, -1, 0))((acc, rock) => {
                    val (newRow, nextStillRock, nPiledUp) = acc
                    val oldCol = rock.col

                    val precedingStillRock = precomputedTilt(oldCol)
                    val newCol =
                        if (precedingStillRock == nextStillRock) precedingStillRock + (nPiledUp + 1) * direction
                        else precedingStillRock + direction
                    val newPiledUp =
                        if (precedingStillRock == nextStillRock) nPiledUp + 1
                        else 1

                    val newRock = new Rock(rock.row, newCol)

                    rocksInCol(newCol) = newRock :: rocksInCol(newCol)

                    (newRock :: newRow, precedingStillRock, newPiledUp)
                })._1
            }

            for {i <- 0 until nRows} {
                rocksInRow(i) = tiltRow(rocksInRow(i), precomputedTilts(i))
            }
        }

        tiltCols( 1, precomputedNorthTilt) // Tilt north 
        tiltRows( 1, precomputedWestTilt) // Tilt west
        tiltCols(-1, precomputedSouthTilt) // Tilt south
        tiltRows(-1, precomputedEastTilt) // Tilt east
    }

    def boardRepresentationAfterCycles: String =  {
        val initialBoard = boardRepresentation(nRows, nCols, rocksInRow, allStillRocks)
        val encounteredBoards = scala.collection.mutable.Map[String, Int]()
        encounteredBoards.addOne((initialBoard, 0))

        def cycleUntilLoop(boards: List[String], iteration: Int): List[String] = {
            singleCycle
            val newBoard = boardRepresentation(nRows, nCols, rocksInRow, allStillRocks)
            
            if (encounteredBoards.contains(newBoard)) boards.reverse
            else {
                encounteredBoards.addOne((newBoard, iteration))
                cycleUntilLoop(newBoard :: boards, iteration + 1)
            }
        }

        val boardsLoop = cycleUntilLoop(List(initialBoard), 1)
        val loopStart = encounteredBoards(boardRepresentation(nRows, nCols, rocksInRow, allStillRocks))
        val finalBoardIndexInLoop = (1000000000 - loopStart) % (boardsLoop.length - loopStart)
        boardsLoop(loopStart + finalBoardIndexInLoop)
    }

    def northLoad: Int = {
        rocksInCol.map(col => {
            col.map(rock => nRows - rock.row).sum
        }).sum
    }
}

def parse(lines: Iterator[String]): Board = {
    val rows: Array[Array[Space]] = lines.map(line => {
        line.map(c => c match {
            case 'O' => RollingRock
            case '#' => StillRock
            case '.' => Empty
            case _ => sys.error("Invalid character")
        }).toArray[Space]
    }).toArray

    new Board(rows)
}

val board = parse(io.Source.stdin.getLines())
val finalBoard = parse(board.boardRepresentationAfterCycles.split("\n").iterator)

val result = finalBoard.northLoad
println(result)
