sealed trait Spring
case object Operational extends Spring
case object Damaged extends Spring

case class SpringRow(row: Array[Option[Spring]], assuredDefined: Int) {
    def nPossibilities(report: List[Int]): Int = {
        def _nPossibilities(acc: Int, toCheck: List[SpringRow]): Int = {
            if (toCheck.isEmpty) acc
            else {
                val nextToCheck = toCheck.head
                val newAcc = acc + nextToCheck.matchesReport(report).map(if (_) 1 else 0).getOrElse(0)
                val newToCheck = nextToCheck.nextPossibilities.map(_ ::: toCheck.tail).getOrElse(toCheck.tail)
                _nPossibilities(newAcc, newToCheck)
            }
        }
        _nPossibilities(0, List(this))
    }

    def isDefined: Boolean = row.drop(assuredDefined).forall(_.isDefined)
    def defined: Option[Array[Spring]] = {
        if (isDefined) Some(row.flatten)
        else None
    }

    def matchesReport(report: List[Int]): Option[Boolean] = {
        autoReport.map(_ == report)
    }

    def autoReport: Option[List[Int]] = {
        defined.map(definedRow => {
            def _countStreaks(acc: List[Int], idx: Int, streak: Int): List[Int] = {
                if (idx >= definedRow.length) {
                    val finalList =
                        if (streak > 0) streak :: acc
                        else acc
                    finalList.reverse
                }
                else {
                    if (definedRow(idx) == Operational)
                        if (streak > 0) _countStreaks(streak :: acc, idx + 1, 0)
                        else _countStreaks(acc, idx + 1, 0)
                    else
                        _countStreaks(acc, idx + 1, streak + 1)
                }
            }
            _countStreaks(Nil, 0, 0)
        })
    }

    def nextUndefined: Option[Int] = {
        val i = row.indexOf(None, assuredDefined)

        if (i >= 0) Some(i)
        else None
    }

    def nextPossibilities: Option[List[SpringRow]] = {
        nextUndefined.map(nextIdx => {
            val newRow1 = row.updated(nextIdx, Some(Operational))
            val newRow2 = row.updated(nextIdx, Some(Damaged))
            List(
                new SpringRow(newRow1, nextIdx),
                new SpringRow(newRow2, nextIdx)
            )
        })
    }

    override def toString(): String = row.map(e => e match {
        case None => "?"
        case Some(Operational) => "."
        case Some(Damaged) => "#"
    }).mkString("")
}

def parse(line: String): (SpringRow, List[Int]) = {
    val lineComponents = line.split("\\s+")

    val rowElements = lineComponents(0)
    val row: Array[Option[Spring]] = rowElements.map(c => c match {
        case '?' => None
        case '#' => Some(Damaged)
        case '.' => Some(Operational)
        case _ => sys.error("Invalid character in row")
    }).toArray

    val reportElements = lineComponents(1)
    val report = reportElements.split(",").map(_.toInt).toList

    (new SpringRow(row, 0), report)
}

val result = io.Source.stdin.getLines().map(parse).map(p =>{
    val (springRow, report) = p
    springRow.nPossibilities(report)
}).sum

println(result)