def squareRootOfPerfectSquare(n: Long): Option[Long] = {
    def _sqrt(lowerBound: Long, upperBound: Long): Option[Long] = {
        val midpoint = (lowerBound + upperBound) / 2
        val midpointSq = (midpoint * midpoint)

        if (midpointSq == n) Some(midpoint)
        else
            if ((midpoint == lowerBound) || (midpoint == upperBound)) None
            else
                if (midpointSq > n) _sqrt(lowerBound, midpoint)
                else _sqrt(midpoint, upperBound)
    }

    if (n == 1) Some(1)
    else _sqrt(0, n)
}

case class Race(totalTime: Long, recordDistance: Long) {
    def waysToWin: Long = {
        val discriminant: Long = (totalTime*totalTime) - 4 * recordDistance

        if (discriminant <= 0) 0
        else {
            squareRootOfPerfectSquare(discriminant).map(sqrt => {
                val tLower = (totalTime - sqrt) / 2
                val tUpper = (totalTime + sqrt) / 2
                tUpper - tLower - 1
            }).getOrElse({
                val sqrt = math.sqrt(discriminant.toDouble)
                val tLower = math.ceil((totalTime - sqrt) / 2).round
                val tUpper = math.floor((totalTime + sqrt) / 2).round
                tUpper - tLower + 1
            })
        }
    }
}

def parseInput(lines: Iterator[String]): Race = {
    val totalTimesString = lines.next()
    val recordDistancesString = lines.next()

    val totalTime = totalTimesString.split(":\\s+")(1).filter(_ != ' ').toLong
    val recordDistance = recordDistancesString.split(":\\s+")(1).filter(_ != ' ').toLong

    new Race(totalTime, recordDistance)
}

val races = parseInput(io.Source.stdin.getLines())
val result = races.waysToWin
println(result)