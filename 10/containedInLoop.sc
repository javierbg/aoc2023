sealed trait Direction {
    def inverse: Direction = this match {
        case North => South
        case East => West
        case South => North
        case West => East
    }

    def movement: (Int, Int) = this match {
        case North => (-1,  0)
        case East  => ( 0, +1)
        case South => (+1,  0)
        case West  => ( 0, -1)
    }
}

case object North extends Direction
case object East extends Direction
case object South extends Direction
case object West extends Direction

case class Pipe(connects: List[Direction]) {
    override def toString: String = {
        if (connects.isEmpty) " "
        else if (connects.contains(North) && connects.contains(South)) "║"
        else if (connects.contains(West) && connects.contains(East)) "═"
        else if (connects.contains(North) && connects.contains(East)) "╚"
        else if (connects.contains(North) && connects.contains(West)) "╝"
        else if (connects.contains(South) && connects.contains(West)) "╗"
        else if (connects.contains(South) && connects.contains(East)) "╔"
        else throw new RuntimeException("")
    }
}

object Pipe {
    def parse(char: Char): Option[Pipe] = {
        if (char == 'S') None
        else new Some(Pipe(char match {
            case '.' => Nil
            case '|' => List(North, South)
            case '-' => List(West, East)
            case 'L' => List(North, East)
            case 'J' => List(North, West)
            case '7' => List(South, West)
            case 'F' => List(South, East)
            case _ => throw new RuntimeException("")
        }))
    }

    
}

case class Map(board: Array[Array[Pipe]]) {
    def apply(i: Int, j: Int): Pipe = {
        if ((i >= board.length) || (i < 0)) new Pipe(Nil)
        else {
            val row = board(i)
            if ((j >= row.length) || (j < 0)) new Pipe(Nil)
            else row(j)
        }
    }

    def update(idx: (Int, Int), value: Pipe): Unit = {
        val (i, j) = idx
        if ((i >= 0) && (i < board.length)) {
            val row = board(i)
            if ((j >= 0) && (j < row.length)) {
                row(j) = value
            }
        }
    }

    def findLoop(startPos: (Int, Int)): List[(Int, Int)] = {
        def loop(currentPos: (Int, Int), lastDirection: Direction, pathTaken: List[(Int, Int)]): List[(Int, Int)] = {
            if ((currentPos == startPos) && !pathTaken.isEmpty) pathTaken
            else {
                val newPathTaken = currentPos :: pathTaken
                val currentPipe = this(currentPos._1, currentPos._2)
                val direction = currentPipe.connects.filter(_ != lastDirection.inverse).head
                val (verticalMovement, horizontalMovement) = direction.movement
                loop(
                    (currentPos._1 + verticalMovement, currentPos._2 + horizontalMovement),
                    direction,
                    newPathTaken
                )
            }
        }

        loop(startPos, this(startPos._1, startPos._2).connects.head.inverse, Nil)
    }

    override def toString(): String = {
        board.map(row => row.map(_.toString).mkString("")).mkString("\n")
    }
}

def indexOf[A](seq: IndexedSeq[A], elem: A): Option[Int] = {
    def _indexOf[A](nextIndex: Int): Option[Int] = {
        if (nextIndex >= seq.length) None
        else if (seq(nextIndex) == elem) Some(nextIndex)
        else _indexOf(nextIndex + 1)
    }
    _indexOf(0)
}

def determineStartPipe(north: Pipe, west: Pipe, south: Pipe, east: Pipe): Pipe = {
    new Pipe(
        (
            (if (north.connects.contains(South)) Some(North) else None) ::
            (if (west.connects.contains(East))   Some(West)  else None) ::
            (if (south.connects.contains(North)) Some(South) else None) ::
            (if (east.connects.contains(West))   Some(East)  else None) :: Nil
        ).flatten
    )
}

def parse(lines: Iterator[String]): (Map, (Int, Int)) = {
    def _parse(acc: List[Array[Pipe]], currentRow: Int, startPos: Option[(Int, Int)]): (Array[Array[Pipe]], (Int, Int)) = {
        if (lines.hasNext) {
            val lineOption = lines.next().map(Pipe.parse)
            val possibleStart = indexOf(lineOption, None)
            val newStartPos = possibleStart.map((currentRow, _)).orElse(startPos)
            val line = lineOption.map(_.getOrElse(new Pipe(Nil))).toArray

            _parse(
                line :: acc,
                currentRow + 1,
                newStartPos
            )
        } else {
            assert(startPos.isDefined)
            (acc.reverse.toArray, startPos.get)
        }
    }

    val (board, startPos) = _parse(Nil, 0, None)
    val map = Map(board)
    map((startPos._1, startPos._2)) = determineStartPipe(
        map(startPos._1-1, startPos._2  ),
        map(startPos._1  , startPos._2-1),
        map(startPos._1+1, startPos._2  ),
        map(startPos._1  , startPos._2+1)
    )
    (map, startPos)
}

def computeArea(startPos: (Int, Int), loop: List[(Int, Int)]): Int = {
    // Shoelace formula
    def _computeArea(acc: Int, currentPos: (Int, Int), restLoop: List[(Int, Int)]): Int = {
        if (restLoop.isEmpty) (acc.abs / 2)
        else {
            val nextPos = restLoop.head
            _computeArea(
                acc + ((currentPos._1 * nextPos._2) - (currentPos._2 * nextPos._1)),
                nextPos,
                restLoop.tail
            )
        }
    }

    _computeArea(0, startPos, loop)
}

def numberIntegerPointsWithinLoop(startPos: (Int, Int), loop: List[(Int, Int)]): Int = {
    // Pick's theorem
    val area = computeArea(startPos, loop)
    area + 1 - (loop.length / 2)
}

val (map, startPos) = parse(io.Source.stdin.getLines())
val loop = map.findLoop(startPos)
val result = numberIntegerPointsWithinLoop(startPos, loop)
println(result)