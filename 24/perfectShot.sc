import scala.language.implicitConversions

val ZERO = BigInt(0)

class Rational(_num: BigInt, _den: BigInt) extends Ordered[Rational] {
    require(_den != 0, "denominator must be non-zero")

    private def gcd(a: BigInt, b: BigInt): BigInt = (if (b == 0) a else gcd(b, a % b)).abs

    val (num, den) = {
        val g = gcd(_num, _den)
        val sign = if ((_num < 0) == (_den < 0)) 1 else -1
        ((_num.abs / g) * sign, _den.abs / g)
    }

    def this(x: BigInt) = this(x, 1)
    def this(_num: Rational, _den: Rational) = this(_num.num * _den.den, _num.den * _den.num)

    def +(other: Rational): Rational = 
        new Rational(num * other.den + other.num * den, den * other.den)

    def unary_- : Rational = new Rational(-num, den)
    def -(other: Rational) =
        new Rational(num * other.den - other.num * den, den * other.den)

    def *(other: Rational) = new Rational(num * other.num, den * other.den)
    def /(other: Rational) = new Rational(num * other.den, den * other.num)

    override def compare(that: Rational): Int =
        (num * that.den) compare (den * that.num)

    def ==(other: Rational): Boolean = ((this compare other) == 0)
    def !=(other: Rational): Boolean = !(this == other)
    
    override def toString(): String = s"$num/$den"
}

implicit def bigIntToRational(x: BigInt): Rational = new Rational(x)

class Trajectory(val initialPos: (BigInt, BigInt, BigInt), val velocity: (BigInt, BigInt, BigInt)) {
    def hits(other: Trajectory): Boolean = {
        // Checks if this trajectory hits another one

        val (x0, y0, z0) = initialPos
        val (vx, vy, vz) = velocity

        val (x0i, y0i, z0i) = other.initialPos
        val (vxi, vyi, vzi) = other.velocity

        if (
            ((x0i != x0) && (vx == vxi)) ||
            ((y0i != y0) && (vy == vyi)) ||
            ((z0i != z0) && (vz == vzi))
        ) false // If same velocity then they cannot hit, unless same initial position
        else {
            // Check the time of the hit in x, y and z.
            // The trajectories hit each other if they are all equal
            
            val tx: Rational = if (x0i == x0) ZERO else ((x0i - x0) / (vx - vxi))
            val ty: Rational = if (y0i == y0) ZERO else ((y0i - y0) / (vy - vyi))

            if (tx != ty) false
            else {
                val tz: Rational = if (z0i == z0) ZERO else ((z0i - z0) / (vz - vzi))
                
                tx == tz
            }
        }
    }
    override def toString(): String = s"${initialPos._1}, ${initialPos._2}, ${initialPos._3} @ ${velocity._1}, ${velocity._2}, ${velocity._3}"
}

object Trajectory {
    def parse(line: String): Trajectory = {
        val lineParts = line.split("\\s*@\\s*")
        val posString = lineParts(0)
        val velString = lineParts(1)

        val posList = posString.split(",\\s*").map(s => BigInt(s))
        val initialPos = (posList(0), posList(1), posList(2))

        val velList = velString.split(",\\s*").map(s => BigInt(s))
        val velocity = (velList(0), velList(1), velList(2))

        new Trajectory(initialPos, velocity)
    }
}

class Spiral extends Iterator[(BigInt, BigInt)] {
    // Spiral iterator centered at the origin
    // This is an infinite iterator that returns coordinate pairs
    // making a spiral.

    var di = BigInt(1)
    var dj = BigInt(0)
    var segment_length = BigInt(1)
    var i = BigInt(0)
    var j = BigInt(0)
    var segment_passed = BigInt(0)

    override def hasNext: Boolean = true

    override def next(): (BigInt, BigInt) = {
        val toReturn = (i, j)

        i += di
        j += dj
        segment_passed += 1

        if (segment_passed == segment_length) {
            segment_passed = 0
            val aux = di
            di = -dj
            dj = aux

            if (dj == 0) segment_length += 1
        }
        toReturn
    }
}

def computePerfectShot(trajectories: List[Trajectory]): Trajectory = {
    // Implementing the original idea by /u/UnicycleBloke:
    // https://www.reddit.com/r/adventofcode/comments/18q7d47/2023_day_24_part_2_a_mathematical_technique_for/keubuig/

    val hail1 = trajectories.head
    val hail2 = trajectories.tail.head
    val restHails = trajectories.tail.tail

    // Use the first two hails as well as two guesses for vx and vy
    // to get the times at which the rock hits them
    val (x01, y01, z01) = hail1.initialPos
    val (vx1, vy1, vz1) = hail1.velocity

    val (x02, y02, z02) = hail2.initialPos
    val (vx2, vy2, vz2) = hail2.velocity

    def computeT1T2(vx: BigInt, vy: BigInt): Option[(Rational, Rational)] = {
        // The differences between the two hails' positions at times t1 and t2
        // are related such that:
        //      (x01 + vx1 * t1) - (x02 + vx2 * t2) = (x0 + vx * t1) - (x0 + vx * t2)
        //      (y01 + vy1 * t1) - (y02 + vy2 * t2) = (y0 + vy * t1) - (y0 + vy * t2)
        // Use these two equations to solve for t1 and t2 and you get the
        // following (expressed as num / den)
        val t1_den = vx*vy1 - vx*vy2 - vx1*vy + vx1*vy2 + vx2*vy - vx2*vy1
        val t2_den = vx*vy1 - vx*vy2 - vx1*vy + vx1*vy2 + vx2*vy - vx2*vy1

        // If any of the denominators are zero, there is no solution
        if ((t1_den == ZERO) || (t2_den == ZERO)) None
        else {
            val t1_num = -vx*y01 + vx*y02 + vx2*y01 - vx2*y02 + vy*x01 - vy*x02 - vy2*x01 + vy2*x02
            val t2_num = -vx*y01 + vx*y02 + vx1*y01 - vx1*y02 + vy*x01 - vy*x02 - vy1*x01 + vy1*x02
            
            val t1 = new Rational(t1_num, t1_den)
            val t2 = new Rational(t2_num, t2_den)
            
            // If t1 or t2 are negative, the rock would have hit the hail in the past,
            // so it is not valid
            if ((t1 < ZERO) || (t2 < ZERO)) None
            else Some((t1, t2))
        }
    }

    def computeVZ(t1: Rational, t2: Rational): Option[BigInt] = {
        val num = (z01 + t1 * vz1) - (z02 + t2 * vz2)
        val den = t1 - t2
        val result = new Rational(num, den)

        // All of the components of the velocities must be integers
        // Otherwise, it is not a valid solution
        if (result.den != 1) None
        else Some(result.num)
    }

    def computeInitialPos(t1: Rational, vx: BigInt, vy: BigInt, vz: BigInt): Option[(BigInt, BigInt, BigInt)] = {
        val x0 = x01 + t1 * (vx1 - vx)
        val y0 = y01 + t1 * (vy1 - vy)
        val z0 = z01 + t1 * (vz1 - vz)

        // All of the components of the initial position must be integers
        // Otherwise, it is not a valid solution
        if ((x0.den != 1) || (y0.den != 1) || (z0.den != 1)) None
        else Some((x0.num, y0.num, z0.num))
    }

    def _computePerfectShot(vx: BigInt, vy: BigInt): Option[Trajectory] = {
        computeT1T2(vx, vy).flatMap({ case (t1, t2) => 
        computeVZ(t1, t2).flatMap(vz => {
        computeInitialPos(t1, vx, vy, vz).flatMap(initialPos => {
            val proposedTrajectory = new Trajectory(initialPos, (vx, vy, vz))

            if (restHails.forall(proposedTrajectory.hits(_))) Some(proposedTrajectory)
            else None
        }) }) })
    }

    // Check all possible integer values for vx and vy starting from the origin in a spiral.
    // Because the correct solution is not far for the origin, it is found fairly quickly.
    val spiral = new Spiral
    spiral.map({case (vx, vy) => _computePerfectShot(vx, vy)}).flatten.take(1).next()
}

val trajectories = io.Source.stdin.getLines().map(Trajectory.parse).toList
val perfectShot = computePerfectShot(trajectories)
println(perfectShot)
val result = perfectShot.initialPos._1 + perfectShot.initialPos._2 + perfectShot.initialPos._3
println(result)