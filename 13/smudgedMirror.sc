sealed trait Symmetry
case class HorizontalSymmetry(cols: Int) extends Symmetry
case class VerticalSymmetry(rows: Int) extends Symmetry


def lineSymmetric(line: Array[Boolean], splitPoint: Int): Boolean = {
    val (left, right) = line.splitAt(splitPoint)
    left.reverse.zip(right).forall(p => p._1 == p._2)
}

case class Board(lines: Array[Array[Boolean]]) {
    require(lines.forall(line => line.length == lines(0).length))

    val nRows = lines.length
    val nCols = lines(0).length

    def transpose: Board = {
        new Board(lines.transpose)
    }

    // Regular symmetry first, just like part one

    def checkRegularHorizontalSymmetry: Option[Int] = {
        def _checkHorizontalSymmetry(next: Int): Option[Int] = {
            if (next >= nCols) None
            else if (checkRegularHorizontalSymmetry(next)) Some(next)
            else _checkHorizontalSymmetry(next+1)
        }
        _checkHorizontalSymmetry(1)
    }

    def checkRegularHorizontalSymmetry(shift: Int): Boolean = {
        lines.forall(line =>{
            val (left, right) = line.splitAt(shift)
            left.reverse.zip(right).forall(p => p._1 == p._2)
        })
    }

    def checkRegularVerticalSymmetry: Option[Int] = transpose.checkRegularHorizontalSymmetry

    def checkRegularSymmetry: Symmetry = {
        checkRegularHorizontalSymmetry.map(HorizontalSymmetry(_)).getOrElse(
            checkRegularVerticalSymmetry.map(VerticalSymmetry(_)).get
        )
    }

    // Smudged symmetry next
    // Sometimes a smudge maintains the original symmetry because it lands
    // in a row or column that gets ignored due to an uneven split.

    def checkSmudgedHorizontalSymmetry: Option[Int] = {
        // In order to check smudged symmetry, first check regular symmetry
        val regularSymmetry = checkRegularHorizontalSymmetry

        def _checkHorizontalSymmetry(next: Int): Option[Int] = {
            if (next >= nCols) None
            else if (checkSmudgedHorizontalSymmetry(next, regularSymmetry)) Some(next)
            else _checkHorizontalSymmetry(next+1)
        }
        _checkHorizontalSymmetry(1)
    }

    def checkSmudgedHorizontalSymmetry(shift: Int, regularSymmetry: Option[Int]): Boolean = {
        def _checkHorizontalSymmetry(nextRow: Int, smudgePending: Boolean): Boolean = {
            // If all rows were checked and no place for a smudge was found => no smudged symmetry
            if (nextRow >= nRows) !smudgePending
            else {
                val line = lines(nextRow)
                if (smudgePending) {
                    def _checkSmudgeSymmetry(nextCol: Int): Boolean = {
                        if (nextCol >= nCols)                           
                            if (lineSymmetric(line, shift)) _checkHorizontalSymmetry(nextRow + 1, true)
                            else false
                        else {
                            val smudgedLine = line.updated(nextCol, !line(nextCol))

                            // The following, although it can be TCO'd would be incorrect:
                            // if (lineSymmetric(smudgedLine, shift)) _checkHorizontalSymmetry(nextRow + 1, false)
                            // else _checkSmudgeSymmetry(nextCol + 1)
                            
                            // In the code above, if the smudged line is symmetric, no other possible smudges
                            // are considered, even if the rest of the rows fail to be symmetric.

                            // If the line is symmetric when smudged and all other rows are symmetric
                            // as well, then a symmetry is found.
                            // Otherwise, the else clause MUST be considered.
                            
                            if (lineSymmetric(smudgedLine, shift) && _checkHorizontalSymmetry(nextRow + 1, false)) true
                            else _checkSmudgeSymmetry(nextCol + 1)
                        }
                    }
                    
                    _checkSmudgeSymmetry(0)
                }
                else { // No pending smudge, just check regular symmetry
                    if (lineSymmetric(line, shift)) _checkHorizontalSymmetry(nextRow + 1, false)
                    else false
                }
            }
        }

        // If this symmetry already exists without a smudge, don't even check
        if (regularSymmetry.map(_ == shift).getOrElse(false)) false
        else _checkHorizontalSymmetry(0, true)
    }

    def checkSmudgedVerticalSymmetry: Option[Int] = transpose.checkSmudgedHorizontalSymmetry

    def checkSmudgedSymmetry: Symmetry = {
        checkSmudgedHorizontalSymmetry.map(HorizontalSymmetry(_)).getOrElse(
            checkSmudgedVerticalSymmetry.map(VerticalSymmetry(_)).get
        )
    }

    override def toString(): String = {
        lines.map(line => line.map({e =>
            if (e) "#" else "."
        }).mkString("")).mkString("\n") + "\n"
    }
}

object Board {
    def parse(lines: Iterator[String]): Board = {
        new Board(lines.takeWhile(!_.isEmpty).map(line => {
            line.map(c => {
                c match {
                    case '#' => true
                    case '.' => false
                    case _ => sys.error(s"Invalid character '${c}'")
                }
            }).toArray
        }).toArray)
    }
}

def parseAll(lines: Iterator[String]): List[Board] = {
    def _parseAll(acc: List[Board]): List[Board] = {
        if (!lines.hasNext) acc.reverse
        else _parseAll(Board.parse(lines) :: acc)
    }
    _parseAll(Nil)
}

def summarize(symmetries: List[Symmetry]): Int = {
    symmetries.map(sym => {
        sym match {
            case HorizontalSymmetry(cols) => cols
            case VerticalSymmetry(rows) => rows*100
        }
    }).sum
}

val boards = parseAll(io.Source.stdin.getLines())
val symmetries = boards.map(_.checkSmudgedSymmetry)
val result = summarize(symmetries)
println(result)