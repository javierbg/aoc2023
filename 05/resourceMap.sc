case class RangeMapping(dstStart: Long, srcStart: Long, length: Long) {
    def map(n: Long): Option[Long] = {
        if ((n >= srcStart) && (n < (srcStart + length)))
            Some(dstStart + (n - srcStart))
        else None
    }

    override def toString(): String = s"${dstStart} ${srcStart} ${length}"
}

case class MapGroup(srcResource: String, dstResource: String, maps: List[RangeMapping]) {
    def map(n: Long): Long = {
        val initial: Option[Long] = None
        maps.foldLeft(initial)((mappedVal: Option[Long], rangeMap: RangeMapping) => {
            if (mappedVal.isEmpty) rangeMap.map(n)
            else mappedVal
        }).getOrElse(n)
    }

    override def toString(): String = s"${srcResource} to ${dstResource}:\n" + maps.mkString("\n")
}

object MapGroup {
    def parse(lines: Iterator[String]): Option[MapGroup] = {
        if (!lines.hasNext) None
        else {
            val firstLine = lines.next()
            val joinedNames = firstLine.split(" ")(0)
            val names = joinedNames.split("-to-")
            val srcName = names(0)
            val dstName = names(1)

            val mappings: List[RangeMapping] = lines.takeWhile(_ != "").map(line =>{
                val mappingVals = line.split("\\s+").map(_.toLong)
                new RangeMapping(mappingVals(0), mappingVals(1), mappingVals(2))
            }).toList

            Some(new MapGroup(srcName, dstName, mappings))
        }
    }
}

case class MapGroupSequence(mapGroups: List[MapGroup]) {
    override def toString(): String = mapGroups.mkString("\n\n")

    def map(n: Long): Long = {
        mapGroups.foldLeft(n)((mappedVal, mapGroup) => mapGroup.map(mappedVal))
    }
}

object MapGroupSequence {
    def parse(lines: Iterator[String]): MapGroupSequence = {
        def _parse(acc: List[MapGroup]): List[MapGroup] = {
            val nextMapGroup = MapGroup.parse(lines)

            if (nextMapGroup.isEmpty) acc
            else _parse(nextMapGroup.get :: acc)
        }

        new MapGroupSequence(_parse(Nil).reverse)
    }
}

def parseSeeds(line: String): Array[Long] = {
    val numbers = line.split(":\\s+")(1)
    numbers.split("\\s+").map(_.toLong)
}

def parseInput(lines: Iterator[String]): (Array[Long], MapGroupSequence) = {
    val seeds = parseSeeds(lines.next())
    lines.next()
    (
        seeds,
        MapGroupSequence.parse(lines)
    )
}

val (seeds, mapGroupSeq) = parseInput(io.Source.stdin.getLines())
val result = seeds.map(mapGroupSeq.map).min
println(result)