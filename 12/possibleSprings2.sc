sealed trait Spring
case object Operational extends Spring
case object Damaged extends Spring

case class SpringRow(row: Array[Option[Spring]], assuredDefined: Int) {
    def nPossibilities(report: List[Int]): Int = {
        val rowList = row.toList
        def _nPossibilities(r: Array[Option[Spring]], currentStreak: Int, toFind: List[Int]): Int = {
            if (r.isEmpty)
                if (
                    (toFind.isEmpty && (currentStreak == 0)) ||
                    ((toFind.length == 1) && (currentStreak == toFind.head))
                ) 1
                else 0
            else {
                r.head match {
                    case None => (
                        _nPossibilities(
                            r.updated(0, Some(Damaged)),
                            currentStreak,
                            toFind,
                        ) + _nPossibilities(
                            r.updated(0, Some(Operational)),
                            currentStreak,
                            toFind,
                        )
                    )
                    case Some(Damaged) => {
                        val newStreak = currentStreak + 1
                        if (toFind.isEmpty || (newStreak > toFind.head)) 0
                        else _nPossibilities(r.drop(1), newStreak, toFind)
                    }
                    case Some(Operational) =>
                        if (currentStreak > 0) {
                            if (toFind.isEmpty || (currentStreak != toFind.head)) 0
                            else _nPossibilities(r.drop(1), 0, toFind.tail)
                        } else _nPossibilities(r.drop(1), 0, toFind)
                }
            }
        }
        _nPossibilities(row, 0, report)
    }

    def isDefined: Boolean = row.drop(assuredDefined).forall(_.isDefined)
    def defined: Option[Array[Spring]] = {
        if (isDefined) Some(row.flatten)
        else None
    }

    override def toString(): String = row.map(e => e match {
        case None => "?"
        case Some(Operational) => "."
        case Some(Damaged) => "#"
    }).mkString("")
}

def parse(line: String): (SpringRow, List[Int]) = {
    val lineComponents = line.split("\\s+")

    val rowElements = lineComponents(0)
    val row: Array[Option[Spring]] = rowElements.map(c => c match {
        case '?' => None
        case '#' => Some(Damaged)
        case '.' => Some(Operational)
        case _ => sys.error("Invalid character in row")
    }).toArray

    val reportElements = lineComponents(1)
    val report = reportElements.split(",").map(_.toInt).toList

    (new SpringRow(row, 0), report)
}

val result = io.Source.stdin.getLines().map(parse).map(p =>{
    val (springRow, report) = p
    val n = springRow.nPossibilities(report)
    println(n)
    n
}).sum

println(result)