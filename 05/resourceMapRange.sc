case class ValueRange(start: Long, length: Long) {
    def end: Long = start + length

    override def toString(): String = s"(${start}, ${start + length})"

    def merge(that: ValueRange): Option[ValueRange] = {
        if (
            (this.start <= that.end) &&
            (that.start <= this.end)
        ) {
            val newStart = this.start.min(that.start)
            val newEnd = this.end.max(that.end)
            val newLength = newEnd - newStart
            Some(new ValueRange(newStart, newLength))
        }
        else None
    }

    def intersection(that: ValueRange): Option[ValueRange] = {
        if (
            (this.start < that.end) &&
            (that.start < this.end)
        ) {
            val newStart = this.start.max(that.start)
            val newEnd = this.end.min(that.end)
            val newLength = newEnd - newStart
            assert(newLength > 0)
            Some(new ValueRange(newStart, newLength))
        }
        else None
    }
}


object ValueRange {
    def consolidate(rs: List[ValueRange]): List[ValueRange] = {
        def _consolidate(acc: List[ValueRange], merging: ValueRange, remaining: List[ValueRange]): List[ValueRange] = {
            if (remaining.isEmpty) merging :: acc
            else {
                val n = merging.merge(remaining.head).map(_ :: acc).getOrElse(remaining.head :: merging :: acc)

                _consolidate(n.tail, n.head, remaining.tail)
            }
        }
        val sortedRanges = rs.toArray.sortBy(_.start).toList
        _consolidate(Nil, sortedRanges.head, sortedRanges.tail)
    }
}

case class RangeMapping(dstStart: Long, srcStart: Long, length: Long) {
    def srcEnd: Long = srcStart + length

    def map(r: ValueRange): (Option[ValueRange], Option[ValueRange], Option[ValueRange]) = {
        val overlapStart = srcStart.max(r.start)
        val overlapEnd = srcEnd.min(r.end)
        val overlapLength = overlapEnd - overlapStart

        val overlap: Option[ValueRange] =
            if (overlapLength > 0) Some(new ValueRange(dstStart + (overlapStart - srcStart), overlapLength))
            else None

        val outsideBeforeEnd = srcStart.min(r.end)
        val outsideBeforeLenght = outsideBeforeEnd - r.start
        val outsideBefore: Option[ValueRange] =
            if (outsideBeforeLenght > 0) Some(new ValueRange(r.start, outsideBeforeLenght))
            else None

        val outsideAfterStart = srcEnd.max(r.start)
        val outsideAfterLength = r.end - outsideAfterStart
        val outsideAfter: Option[ValueRange] =
            if (outsideAfterLength > 0) Some(new ValueRange(outsideAfterStart, outsideAfterLength))
            else None

        (outsideBefore, overlap, outsideAfter)
    }

    override def toString(): String = s"(${dstStart} ${srcStart} ${length})"
}

case class MapGroup(srcResource: String, dstResource: String, maps: List[RangeMapping]) {
    def map(r: ValueRange): List[ValueRange] = {
        def _map(acc: List[ValueRange], prevAfter: Option[ValueRange], remaingingMaps: List[RangeMapping], isFirst: Boolean): List[ValueRange] = {
            val nextRange = remaingingMaps.head
            val (outsideBefore, overlap, outsideAfter) = nextRange.map(r)
            val acc1 = overlap.map(_ :: acc).getOrElse(acc)

            val acc2 =
                if (isFirst && outsideBefore.isDefined) outsideBefore.get :: acc1
                else acc1

            val acc3 =
                if (prevAfter.isDefined && outsideBefore.isDefined)
                    prevAfter.get.intersection(outsideBefore.get).map(_ :: acc2).getOrElse(acc2)
                else acc2
            
            if (remaingingMaps.tail.isEmpty) outsideAfter.map(_ :: acc3).getOrElse(acc3)
            else _map(acc3, outsideAfter, remaingingMaps.tail, false)
        }

        val ranges = _map(Nil, None, maps, true)
        
        assert(r.length == (ranges.map(_.length).sum))
        ranges
    }

    override def toString(): String = s"${srcResource} to ${dstResource}:\n" + maps.mkString("\n")
}

object MapGroup {
    def parse(lines: Iterator[String]): Option[MapGroup] = {
        if (!lines.hasNext) None
        else {
            val firstLine = lines.next()
            val joinedNames = firstLine.split(" ")(0)
            val names = joinedNames.split("-to-")
            val srcName = names(0)
            val dstName = names(1)

            val mappings: List[RangeMapping] = lines.takeWhile(_ != "").map(line =>{
                val mappingVals = line.split("\\s+").map(_.toLong)
                new RangeMapping(mappingVals(0), mappingVals(1), mappingVals(2))
            }).toArray.sortBy(_.srcStart).toList

            Some(new MapGroup(srcName, dstName, mappings))
        }
    }
}

case class MapGroupSequence(mapGroups: List[MapGroup]) {
    override def toString(): String = mapGroups.mkString("\n\n")

    def map(r: List[ValueRange]): List[ValueRange] = {
        val ranges = mapGroups.foldLeft(r)((mappedRanges, mapGroup) => mappedRanges.map(mapGroup.map).flatten)
        ValueRange.consolidate(ranges)
    }
}

object MapGroupSequence {
    def parse(lines: Iterator[String]): MapGroupSequence = {
        def _parse(acc: List[MapGroup]): List[MapGroup] = {
            val nextMapGroup = MapGroup.parse(lines)

            if (nextMapGroup.isEmpty) acc
            else _parse(nextMapGroup.get :: acc)
        }

        new MapGroupSequence(_parse(Nil).reverse)
    }
}


def parseSeeds(line: String): List[ValueRange] = {
    val numbers = line.split(":\\s+")(1)
    numbers.split("\\s+").map(_.toLong).grouped(2).map(l => new ValueRange(l(0), l(1))).toList
}

def parseInput(lines: Iterator[String]): (List[ValueRange], MapGroupSequence) = {
    val seeds = parseSeeds(lines.next())
    lines.next()
    (
        seeds,
        MapGroupSequence.parse(lines)
    )
}

val (seeds, mapGroupSeq) = parseInput(io.Source.stdin.getLines())
val resultRanges = mapGroupSeq.map(seeds)
val result = resultRanges.map(_.start).min
println(result)