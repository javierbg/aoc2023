'''
An alternate solution to day 24 (part two) using the Z3 theorem prover.

This is technically correct, but it takes too long for the problem input.
'''

from z3 import *

def main():
    hails = list()
    while True:
        try:
            line = input()
            pos_str, vel_str = line.split("@")
            pos = tuple(map(lambda s: int(s.strip()), pos_str.split(",")))
            vel = tuple(map(lambda s: int(s.strip()), vel_str.split(",")))
            hails.append((pos, vel))
        except EOFError:
            break

    set_param("parallel.enable", True)
    set_param("parallel.threads.max", 4)
    m = Model()
    x0 = Int('x0')
    y0 = Int('y0')
    z0 = Int('z0')

    vx = Int('vx')
    vy = Int('vy')
    vz = Int('vz')

    tis = [Real(f't{i}') for i, _ in enumerate(hails)]

    constraints = [ti >= 0 for ti in tis]
    for hail, ti in zip(hails, tis):    
        (x0i, y0i, z0i), (vxi, vyi, vzi) = hail
        constraints += [
            (x0 + vx * ti) == (x0i + vxi * ti),
            (y0 + vy * ti) == (y0i + vyi * ti),
            (z0 + vz * ti) == (z0i + vzi * ti),
        ]
    solve(*constraints)
    


if __name__ == '__main__':
    main()
