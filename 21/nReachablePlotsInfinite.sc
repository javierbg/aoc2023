case class Plot(pos: (Int, Int), neighbours: Set[Direction])

sealed trait Direction {
    val movement: (Int, Int)
    def move(pos: (Int, Int)): (Int, Int) =
        (pos._1 + movement._1, pos._2 + movement._2)
}
object Left extends Direction {
    override val movement: (Int, Int) = (0, -1)
    override def toString(): String = "Left"
}
object Right extends Direction{
    override val movement: (Int, Int) = (0, 1)
    override def toString(): String = "Right"
}
object Up extends Direction{
    override val movement: (Int, Int) = (-1, 0)
    override def toString(): String = "Up"
}
object Down extends Direction{
    override val movement: (Int, Int) = (1, 0)
    override def toString(): String = "Down"
}

def parse(lines: Iterator[String]): (Int, Int, Map[(Int, Int), Plot], (Int, Int)) = {
    val mapArray: Array[Array[Char]] = lines.map(_.toArray).toArray
    assert(mapArray.forall(row => row.length == mapArray(0).length))

    val nRows = mapArray.length
    val nCols = mapArray(0).length

    def plotPresentAt(row: Int, col: Int): Boolean =
        mapArray(Math.floorMod(row, nRows))(Math.floorMod(col, nCols)) != '#'

    def parseLine(row: Int, col: Int, acc: List[Plot], startingPos: Option[(Int, Int)]): (List[Plot], Option[(Int, Int)]) = {
        if (col >= nCols) (acc, startingPos)
        else {
            val nextPlotChar = mapArray(row)(col)

            nextPlotChar match {
                case '.' | 'S' => {
                    val neighbours: Set[Direction] = List(
                        if (plotPresentAt(row - 1, col)) Some(Up) else None,
                        if (plotPresentAt(row + 1, col)) Some(Down) else None,
                        if (plotPresentAt(row, col - 1)) Some(Left) else None,
                        if (plotPresentAt(row, col + 1)) Some(Right) else None,
                    ).flatten.toSet

                    val newStartingPos =
                        if (nextPlotChar == 'S') Some((row, col))
                        else startingPos
                    
                    parseLine(row, col+1, new Plot((row, col), neighbours) :: acc, newStartingPos)
                }
                case '#' => parseLine(row, col+1, acc, startingPos)
                case _ => sys.error("Invalid character")
            }
        }
    }

    def parseLines(row: Int, acc: List[Plot], startingPos: Option[(Int, Int)]): (List[Plot], (Int, Int)) = {
        if (row >= nRows) (acc, startingPos.get)
        else {
            val (newAcc, newStartingPos) = parseLine(row, 0, acc, startingPos)
            parseLines(row + 1, newAcc, newStartingPos)
        }
    }
    
    val (plots, startingPos) = parseLines(0, Nil, None)
    val plotsMap = (for {plot <- plots} yield plot.pos -> plot).toMap
    
    (nRows, nCols, plotsMap, startingPos)
}

def nReachableIn(nRows: Int, nCols: Int, plots: Map[(Int, Int), Plot], startingPos: (Int, Int), steps: Int): Long = {
    // Solution by /u/villi_ :
    // https://www.reddit.com/r/adventofcode/comments/18nol3m/2023_day_21_a_geometric_solutionexplanation_for/

    assert(nRows == nCols)
    val dim = nRows
    assert((dim % 2) == 1)
    val nToEdge = dim / 2
    assert((steps % dim) == nToEdge)

    val shortestDistance = collection.mutable.HashMap[(Int, Int), Int]((startingPos, 0))
    
    def isInsideMap(pos: (Int, Int)): Boolean =
        (pos._1 >= 0) && (pos._1 < nRows) && (pos._2 >= 0) && (pos._2 < nCols)

    def manhattanDistance(p1: (Int, Int), p2: (Int, Int)): Int =
        (p1._1 - p2._1).abs + (p1._2 - p2._2).abs

    val frontier = collection.mutable.Set[(Int, Int)](startingPos)
    val visited = collection.mutable.Set[(Int, Int)]()
    def computeShortestDistance(): Unit = {
        if (frontier.isEmpty) return
        
        val plotPos = frontier.head
        frontier.remove(plotPos)
        visited.add(plotPos)

        val plot = plots(plotPos)
        val sd = shortestDistance(plot.pos)
        val neighbours = plot.neighbours.map(direction => {
            val neighbourPos = direction.move(plot.pos)
            if (isInsideMap(neighbourPos)) {
                shortestDistance.updateWith((neighbourPos))(d =>
                    d.map(_ min (sd+1)).orElse(Some(sd+1))
                )

                if (!visited.contains(neighbourPos)) frontier.add(neighbourPos)
            }
        })
        computeShortestDistance()
    }
    computeShortestDistance()

    val even_corners = shortestDistance.count(kv => {
        val (p, d) = kv
        ((d % 2) == 0) && (d > nToEdge) && (manhattanDistance(p, startingPos) > nToEdge)
    }).toLong
    val odd_corners = shortestDistance.count(kv => {
        val (p, d) = kv
        ((d % 2) == 1) && (d > nToEdge) && (manhattanDistance(p, startingPos) > nToEdge)
    }).toLong

    val even_full = shortestDistance.values.count(d => (d % 2) == 0).toLong
    val odd_full  = shortestDistance.values.count(d => (d % 2) == 1).toLong

    val n: Long = steps / dim

    ((n+1)*(n+1)) * odd_full + (n*n) * even_full - (n+1) * odd_corners + n * even_corners
}

val (nRows, nCols, plots, startingPos) = parse(io.Source.stdin.getLines())
val result = nReachableIn(nRows, nCols, plots, startingPos, 26501365)
println(result)