// Implementation linear in the grid size
// Idea by: https://www.reddit.com/r/adventofcode/comments/18h1hho/2023_day_11_a_more_challenging_input_complexity/?ref=share&ref_source=link

// Change to 2 for part one version
val EXPANSION: Long = 1000000L

class SkyMap(image: Array[Array[Boolean]]) {
    require(image.forall(row => row.length == image(0).length))

    val nRows = image.length
    val nCols = image(0).length

    val galaxies: List[(Int, Int)] = {
        (for {
            (row, i) <- image.zipWithIndex
            (isGalaxy, j) <- row.zipWithIndex
        } yield {
            if (isGalaxy) Some((i, j))
            else None
        }).flatten.toList
    }

    val nGalaxies = galaxies.length

    val (rowBins, colBins): (Array[Int], Array[Int]) = {
        val rowCounts = Array.ofDim[Int](nRows)
        val colCounts = Array.ofDim[Int](nRows)
        galaxies.foreach(galaxy => {
            rowCounts(galaxy._1) += 1
            colCounts(galaxy._2) += 1
        })
        (rowCounts, colCounts)
    }

    def baseDistance(bins: Array[Int]): Long = {
        def _accumulateDistance(acc: Long, currentBin: Int, nGalaxiesSoFar: Int): Long = {
            if (currentBin >= bins.length) acc 
            else {
                val newNGalaxies = nGalaxiesSoFar + bins(currentBin)
                val newAcc = acc + (nGalaxiesSoFar until newNGalaxies).map(
                    k => (2*(k+1) - nGalaxies - 1).toLong * currentBin
                ).sum
                _accumulateDistance(newAcc, currentBin+1, newNGalaxies)
            }
        }
        _accumulateDistance(0L, 0, 0)
    }

    def rowBaseDistance: Long = baseDistance(rowBins)
    def colBaseDistance: Long = baseDistance(colBins)

    def expansionCoefficient(bins: Array[Int]): Long = {
        def _expansionCoefficient(acc: Long, possibleBlankIndex: Int, nBefore: Int, nAfter: Int): Long = {
            if (possibleBlankIndex >= bins.length) acc
            else {
                val inBin = bins(possibleBlankIndex)

                if (inBin == 0)
                    _expansionCoefficient(
                        acc + (nBefore * nAfter),
                        possibleBlankIndex + 1,
                        nBefore,
                        nAfter
                    )
                else
                    _expansionCoefficient(
                        acc,
                        possibleBlankIndex + 1,
                        nBefore + inBin,
                        nAfter - inBin
                    )
            }
        }
        
        _expansionCoefficient(0L, 0, 0, nGalaxies)
    }

    def rowExpansionCoefficient: Long = expansionCoefficient(rowBins)
    def colExpansionCoefficient: Long = expansionCoefficient(colBins)

    def totalDistanceBetweenGalaxies: Long = {
        val rowDistance = rowBaseDistance + (EXPANSION - 1) * rowExpansionCoefficient
        val colDistance = colBaseDistance + (EXPANSION - 1) * colExpansionCoefficient
        rowDistance + colDistance
    }

    override def toString(): String = {
        image.map(row => {
            row.map(isGalaxy =>
                if (isGalaxy) '#'
                else '.'
            ).mkString("")
        }).mkString("\n")
    }
}

object SkyMap {
    def parse(lines: Iterator[String]): SkyMap = {
        new SkyMap(
            lines.map(line => line.map(_ == '#').toArray).toArray
        )
    }
}

val skyMap = SkyMap.parse(io.Source.stdin.getLines())
val result = skyMap.totalDistanceBetweenGalaxies
println(result)