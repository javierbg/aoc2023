import scala.util.Sorting.quickSort

sealed trait Card extends Ordered[Card]{
    def numberValue: Int = {
        this match {
            case Ace => 14
            case King => 13
            case Queen => 12
            case Jack => 11
            case NumberedCard(n) => n
        }
    }

    def compare(that: Card): Int = this.numberValue - that.numberValue

    override def toString(): String = this match {
        case Ace => "A"
        case King => "K"
        case Queen => "Q"
        case Jack => "J"
        case NumberedCard(10) => "T"
        case NumberedCard(n) => n.toString()
    }
}

case object Ace extends Card
case object King extends Card
case object Queen extends Card
case object Jack extends Card
case class NumberedCard(n: Int) extends Card

object Card {
    val types: List[Card] = List(
        Ace, King, Queen, Jack
    ) ++ (for (n <- 10 to 2 by -1) yield NumberedCard(n)).toList

    def parse(char: Char): Card = char match {
        case 'A' => Ace
        case 'K' => King
        case 'Q' => Queen
        case 'J' => Jack
        case 'T' => NumberedCard(10)
        case c => NumberedCard(c.toInt - 48)
    }
}

sealed trait HandType extends Ordered[HandType] {
    def numberValue: Int =  {
        this match {
            case FiveOfAKind => 7
            case FourOfAKind => 6
            case FullHouse => 5
            case ThreeOfAKind => 4
            case TwoPairs => 3
            case OnePair => 2
            case HighCard => 1
        }
    }

    def compare(that: HandType): Int = this.numberValue - that.numberValue
}
case object FiveOfAKind extends HandType
case object FourOfAKind extends HandType
case object FullHouse extends HandType
case object ThreeOfAKind extends HandType
case object TwoPairs extends HandType
case object OnePair extends HandType
case object HighCard extends HandType

case class Hand(cards: (Card, Card, Card, Card, Card), bid: Int) extends Ordered[Hand] {
    val cardCounts: Map[Card, Int] = 
        (for (cardType <- Card.types) yield
            cardType -> Hand.countCards(this, cardType)).toMap

    val handType: HandType = {
        cardCounts match {
            case counts if counts.valuesIterator.contains(5) => FiveOfAKind
            case counts if counts.valuesIterator.contains(4) => FourOfAKind
            case counts if (counts.valuesIterator.contains(3) && counts.valuesIterator.contains(2)) => FullHouse
            case counts if (counts.valuesIterator.contains(3)) => ThreeOfAKind
            case counts if (counts.valuesIterator.count(_ == 2) == 2) => TwoPairs
            case counts if (counts.valuesIterator.contains(2)) => OnePair
            case _ => HighCard
        }
    }

    def compare(that: Hand): Int = {
        val handComparison = this.handType.compare(that.handType)
        if (handComparison != 0) handComparison
        else this.cards.compare(that.cards)
    }
    
    override def toString(): String = {
        cards._1.toString() +
        cards._2.toString() +
        cards._3.toString() +
        cards._4.toString() +
        cards._5.toString() +
        " " + bid.toString()
    }
}

object Hand {
    def countCards(hand: Hand, cardType: Card): Int = {
        (if (hand.cards._1 == cardType) 1 else 0) +
        (if (hand.cards._2 == cardType) 1 else 0) +
        (if (hand.cards._3 == cardType) 1 else 0) +
        (if (hand.cards._4 == cardType) 1 else 0) +
        (if (hand.cards._5 == cardType) 1 else 0)
    }

    def parseCards(cardsPart: String): (Card, Card, Card, Card, Card) = {
        (
            Card.parse(cardsPart.charAt(0)),
            Card.parse(cardsPart.charAt(1)),
            Card.parse(cardsPart.charAt(2)),
            Card.parse(cardsPart.charAt(3)),
            Card.parse(cardsPart.charAt(4)),
        )
    }

    def parseLine(line: String): Hand = {
        val lineParts =  line.split("\\s+")
        val cardsPart = lineParts(0)
        val bid = lineParts(1).toInt
        new Hand(parseCards(cardsPart), bid)
    }
}

val hands: Array[Hand] = io.Source.stdin.getLines().map(Hand.parseLine).toArray
quickSort(hands)
val result = hands.zipWithIndex.map(p => (p._2 + 1) * p._1.bid).sum
println(result)