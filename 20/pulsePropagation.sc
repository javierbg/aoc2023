val VERBOSE = true

sealed trait PulseType
object LowPulse extends PulseType {
    override def toString(): String = "low"
}
object HighPulse extends PulseType {
    override def toString(): String = "high"
}

case class Pulse(from: String, to: String, pulseType: PulseType, sentOn: Int) {
    pulseType match {
        case HighPulse => Pulse.nGeneratedHigh += 1
        case LowPulse => Pulse.nGeneratedLow += 1
    }
}

object TimeOrder extends Ordering[Pulse] {
    def compare(x: Pulse, y: Pulse): Int = y.sentOn compare x.sentOn
}

object Pulse {
    var nGeneratedLow: Int = 0
    var nGeneratedHigh: Int = 0
}

sealed trait PulseModule {
    val name: String
    val outputModules: List[String]
    def receive(pulse: Pulse): List[Pulse] = {
        if (VERBOSE) println(s"${pulse.from} -${pulse.pulseType}-> ${pulse.to}")
        Nil
    }

    protected def sendAll(pulseType: PulseType, on: Int): List[Pulse] = {
        for { outputModule <- outputModules } yield
            { new Pulse(from = name, to = outputModule, pulseType = pulseType, sentOn = on) }
    }
}

class FlipFlopModule(_name: String, _outputModules: List[String]) extends PulseModule {
    override val name: String = _name

    override val outputModules: List[String] = _outputModules
    var on: Boolean = false

    override def receive(pulse: Pulse): List[Pulse] = {
        super.receive(pulse)

        pulse.pulseType match {
            case HighPulse => Nil
            case LowPulse => {
                on = !on
                
                val outputType: PulseType =
                    if (on) HighPulse
                    else LowPulse

                sendAll(outputType, pulse.sentOn + 1)
            }
        }
    }
}

class ConjuctionModule(_name: String, inputModules: List[String], _outputModules: List[String]) extends PulseModule {
    override val name: String = _name

    override val outputModules: List[String] = _outputModules

    val mostRecent = collection.mutable.Map[String, PulseType](
        (for { moduleName <- inputModules } yield moduleName -> LowPulse).toList:_*
    )

    override def receive(pulse: Pulse): List[Pulse] = {
        super.receive(pulse)
        
        mostRecent(pulse.from) = pulse.pulseType

        val outputType: PulseType =
            if (mostRecent.values.forall(_ == HighPulse)) LowPulse
            else HighPulse
        
        sendAll(outputType, pulse.sentOn + 1)
    }
}

class BroadcastModule(_name: String, _outputModules: List[String]) extends PulseModule {
    override val name: String = _name

    override val outputModules: List[String] = _outputModules

    override def receive(pulse: Pulse): List[Pulse] = {
        super.receive(pulse)
        
        sendAll(pulse.pulseType, pulse.sentOn + 1)
    }
}

sealed trait ModuleType
object FlipFlop extends ModuleType
object Conjunction extends ModuleType
object Broadcast extends ModuleType

type Network = Map[String, PulseModule]

def parse(lines: Iterator[String]): Network = {
    val modulesPrototypes: Map[String, (ModuleType, List[String])] =
        (for { line <- lines } yield {
            val parts = line.split(" -> ")
            val lhs = parts(0)
            val rhs = parts(1)
            val (moduleType, moduleName) = lhs.charAt(0) match {
                case '%' => (FlipFlop, lhs.drop(1))
                case '&' => (Conjunction, lhs.drop(1))
                case _ => {
                    assert(lhs == "broadcaster")
                    (Broadcast, lhs)
                }
            }
            val outputs = rhs.split(", ").toList

            moduleName -> (moduleType, outputs)
        }).toMap

    (for { (moduleName, (moduleType, outputs)) <- modulesPrototypes } yield {
        val module: PulseModule = moduleType match {
            case FlipFlop => new FlipFlopModule(moduleName, outputs)
            case Conjunction => {
                val connectedModules: List[String] =
                    modulesPrototypes.filter(p => {
                        val (_, (_, otherOutputs)) = p
                        otherOutputs.contains(moduleName)
                    }).map(p => {
                        val (otherName, (_, _)) = p
                        otherName
                    }).toList

                new ConjuctionModule(moduleName, connectedModules, outputs)
            }
            case Broadcast => new BroadcastModule(moduleName, outputs)
        }

        moduleName -> module
    }).toMap
}

def pushButton(network: Network): Unit = {
    val pulseQueue = collection.mutable.PriorityQueue[Pulse](
        new Pulse(from = "button", to = "broadcaster", pulseType = LowPulse, sentOn = 0)
    )(TimeOrder)

    def _pushButton(): Unit = {
        if (pulseQueue.isEmpty) ()
        else {
            val nextPulse = pulseQueue.dequeue()
            val destination = network(nextPulse.to)
            
            val newPulses = destination.receive(nextPulse)
            pulseQueue.enqueue(newPulses.filter(p => network.contains(p.to)):_*)
            _pushButton()
        }
    }

    _pushButton()
}

val network = parse(io.Source.stdin.getLines())
(1 to 1000).foreach(_ => pushButton(network))
val result = Pulse.nGeneratedLow * Pulse.nGeneratedHigh
println(result)