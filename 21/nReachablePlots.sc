case class Plot(pos: (Int, Int), neighbours: Set[(Int, Int)])

def parse(lines: Iterator[String]): (Map[(Int, Int), Plot], (Int, Int)) = {
    val mapArray: Array[Array[Char]] = lines.map(_.toArray).toArray
    assert(mapArray.forall(row => row.length == mapArray(0).length))

    val nRows = mapArray.length
    val nCols = mapArray(0).length

    def getPlotAt(row: Int, col: Int): Option[(Int, Int)] =
        if ((row < 0) || (row >= nRows) || (col < 0) || (col >= nCols) || (mapArray(row)(col) == '#'))
            None
        else Some((row, col))

    def parseLine(row: Int, col: Int, acc: List[Plot], startingPos: Option[(Int, Int)]): (List[Plot], Option[(Int, Int)]) = {
        if (col >= nCols) (acc, startingPos)
        else {
            val nextPlotChar = mapArray(row)(col)

            nextPlotChar match {
                case '.' | 'S' => {
                    val neighbours = List(
                        getPlotAt(row - 1, col),
                        getPlotAt(row + 1, col),
                        getPlotAt(row, col - 1),
                        getPlotAt(row, col + 1)
                    ).flatten.toSet

                    val newStartingPos =
                        if (nextPlotChar == 'S') Some((row, col))
                        else startingPos
                    
                    parseLine(row, col+1, new Plot((row, col), neighbours) :: acc, newStartingPos)
                }
                case '#' => parseLine(row, col+1, acc, startingPos)
                case _ => sys.error("Invalid character")
            }
        }
    }

    def parseLines(row: Int, acc: List[Plot], startingPos: Option[(Int, Int)]): (List[Plot], (Int, Int)) = {
        if (row >= nRows) (acc, startingPos.get)
        else {
            val (newAcc, newStartingPos) = parseLine(row, 0, acc, startingPos)
            parseLines(row + 1, newAcc, newStartingPos)
        }
    }
    
    val (plots, startingPos) = parseLines(0, Nil, None)
    val plotsMap = (for {plot <- plots} yield plot.pos -> plot).toMap
    
    (plotsMap, startingPos)
}

def nReachableIn(plots: Map[(Int, Int), Plot], startingPos: (Int, Int), steps: Int): Int = {
    val queue = collection.mutable.LinkedHashSet[((Int, Int), Int)]( (startingPos, 0) )

    def advance(lastSteps: Int, acc: Int): Int = {
        val (currentPlotPos, currentPlotTime) = queue.head
        queue.remove(queue.head)

        if (currentPlotTime > steps) acc
        else {
            val newAcc =
                if (currentPlotTime != lastSteps) {
                    1
                }
                else acc + 1
            
            val currentPlot = plots(currentPlotPos)
            
            currentPlot.neighbours.foreach(neighbouringPlot => {
                queue.add((neighbouringPlot, currentPlotTime+1))
            })

            advance(currentPlotTime, newAcc)
        }
    }

    advance(0, 0)
}

val (plots, startingPos) = parse(io.Source.stdin.getLines())
val result = nReachableIn(plots, startingPos, 64)
println(result)