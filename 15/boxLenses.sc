import java.util.LinkedList

sealed trait Operation {
    val box: Int = {
        this match {
            case Remove(label) => hashLabel(label)
            case Relabel(label, _) => hashLabel(label)
        }
    }
}
case class Remove(label: String) extends Operation
case class Relabel(label: String, focalLength: Int) extends Operation

object Operation {
    def parse(s: String): Operation = {
        if (s.contains('-')) new Remove(s.dropRight(1))
        else {
            val parts = s.split("=")
            new Relabel(parts(0), parts(1).toInt)
        }
    }
}

def hashLabel(s: String): Int = {
    def _hash(acc: Int, remaining: String): Int = {
        if (remaining.isEmpty) acc
        else {
            val v1 = (acc + remaining.head.toInt) % 256
            val v2 = (v1 * 17) % 256
            _hash(v2, remaining.tail)
        }
    }
    _hash(0, s)
}

case class Lens(label: String, focalLength: Int)

def indexOf[A](seq: LinkedList[A], pred: A => Boolean): Option[java.util.ListIterator[A]] = {
    val it = seq.listIterator()
    def _indexOf: Option[java.util.ListIterator[A]] = {
        if (it.hasNext()) {
            if (pred(it.next())) Some(it)
            else _indexOf
        } else None
    }
    _indexOf
}

val input: String = io.Source.stdin.getLines().next()
val operations = input.split(",").map(Operation.parse)
val boxes: Array[LinkedList[Lens]] = (for { _ <- 0 until 256 } yield new java.util.LinkedList[Lens]()).toArray
operations.foreach(op => {
    val box = boxes(op.box)

    op match {
        case Remove(label) => {
            indexOf(box, (lens: Lens) => lens.label == label).foreach(it => it.remove())
        }
        case Relabel(label, focalLength) => {
            indexOf(box, (lens: Lens) => lens.label == label) match {
                case Some(it) => {
                    it.remove()
                    it.add(new Lens(label, focalLength))
                }
                case None => {
                    box.add(new Lens(label, focalLength))
                }
            }
        }
    }
})
val result = boxes.zipWithIndex.map(p => {
    val (box, boxIdx) = p
    val it = box.iterator()
    var acc = 0
    var lensIdx = 0

    while (it.hasNext()) {
        acc += (1 + boxIdx) * (1 + lensIdx) * it.next.focalLength
        lensIdx += 1
    }

    acc
}).sum
println(result)