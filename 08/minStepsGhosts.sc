case class Node(name: String)

sealed trait Move
case object Left  extends Move
case object Right extends Move

object Move {
    def parsMoves(line: String): Seq[Move] = {
        line.map(c => {
            if (c == 'L') Left
            else if (c == 'R') Right
            else throw new RuntimeException()
        })
    }
}

case class Cycle(visitedDestinations: List[(Node, Int)], length: Int)

case class Network(paths: Map[Node, (Node, Node)]) {
    def thereAndBackAgain(source: Node, destinationValidator: Node => Boolean, moveSeq: Seq[Move]): Cycle = {
        def _thereAndBackAgain(nMoves: Int, currentPosition: Node, visitedPositions: collection.mutable.Map[(Node, Int), Int]): Cycle = {
            val nextMoveIndex = nMoves % moveSeq.length

            if (visitedPositions.contains((currentPosition, nextMoveIndex)))
                new Cycle(
                    visitedPositions.keys.filter(pair => destinationValidator(pair._1)).toList,
                    nMoves - visitedPositions((currentPosition, nextMoveIndex))
                )
            else {
                visitedPositions((currentPosition, nextMoveIndex)) = nMoves
                val nextMove = moveSeq(nextMoveIndex)
                val newPosition =
                    if (nextMove == Left) paths(currentPosition)._1
                    else paths(currentPosition)._2
                _thereAndBackAgain(nMoves + 1, newPosition, visitedPositions)
            }
        }

        _thereAndBackAgain(0, source, collection.mutable.HashMap[(Node, Int), Int]())
    }
}

object Network {
    def parse(lines: Iterator[String]): Network  = {
        new Network((for (line <- lines) yield {
            val lineParts = line.split(" = ")
            val position = new Node(lineParts(0))
            val moves = lineParts(1).substring(1, lineParts(1).length - 1).split(", ")
            position -> (new Node(moves(0)), new Node(moves(1)))
        }).toMap)
    }
}

def parse(lines: Iterator[String]): (Seq[Move], Network) = {
    val moves = Move.parsMoves(lines.next())
    lines.next()
    val network = Network.parse(lines)
    (moves, network)
}

def gcd(a: Long, b: Long): Long = {
    if (b == 0) a else gcd(b, a % b)
}

def lcm(ns: Iterable[Long]): Long = {
    def _lcm(a: Long, b: Long): Long = a * (b / gcd(a, b))
    ns.foldLeft(1L)(_lcm)
}

val (moves, netowrk) = parse(io.Source.stdin.getLines())
val srcNodes = netowrk.paths.keys.filter(_.name.endsWith("A"))
val destinationValidator = (node: Node) => node.name.endsWith("Z")
val dstNodes = netowrk.paths.keys.filter(destinationValidator)
val cycles = srcNodes.map(netowrk.thereAndBackAgain(_, destinationValidator, moves))

// Only works if the following is true
// This is true for the input file but not for the sample inputs >:(
assert(cycles.forall(cycle => {
    (cycle.visitedDestinations.length == 1) &&
    (cycle.visitedDestinations(0)._2 == 0)
}))
assert(cycles.map(_.visitedDestinations(0)._1).toSet == dstNodes.toSet)
val result = lcm(cycles.map(_.length.toLong))
println(result)