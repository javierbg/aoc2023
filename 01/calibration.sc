val result: Int = io.Source.stdin.getLines()
    .map(line => line.filter(_.isDigit))
    .map(digits => {
        s"${digits(0)}${digits(digits.length - 1)}"
    })
    .map(_.toInt)
    .sum
println(result)