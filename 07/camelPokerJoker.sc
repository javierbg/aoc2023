import scala.util.Sorting.quickSort

sealed trait Card extends Ordered[Card]{
    def numberValue: Int = {
        this match {
            case Ace => 14
            case King => 13
            case Queen => 12
            case NumberedCard(n) => n
            case Joker => 1
        }
    }

    def compare(that: Card): Int = this.numberValue - that.numberValue

    override def toString(): String = this match {
        case Ace => "A"
        case King => "K"
        case Queen => "Q"
        case Joker => "J"
        case NumberedCard(10) => "T"
        case NumberedCard(n) => n.toString()
    }
}

case object Ace extends Card
case object King extends Card
case object Queen extends Card
case object Joker extends Card
case class NumberedCard(n: Int) extends Card

object Card {
    val types: List[Card] = List(
        Ace, King, Queen, Joker
    ) ++ (for (n <- 10 to 2 by -1) yield NumberedCard(n)).toList

    def parse(char: Char): Card = char match {
        case 'A' => Ace
        case 'K' => King
        case 'Q' => Queen
        case 'J' => Joker
        case 'T' => NumberedCard(10)
        case c => NumberedCard(c.toInt - 48)
    }
}

sealed trait HandType extends Ordered[HandType] {
    def numberValue: Int =  {
        this match {
            case FiveOfAKind => 7
            case FourOfAKind => 6
            case FullHouse => 5
            case ThreeOfAKind => 4
            case TwoPairs => 3
            case OnePair => 2
            case HighCard => 1
        }
    }

    def compare(that: HandType): Int = this.numberValue - that.numberValue
}
case object FiveOfAKind extends HandType
case object FourOfAKind extends HandType
case object FullHouse extends HandType
case object ThreeOfAKind extends HandType
case object TwoPairs extends HandType
case object OnePair extends HandType
case object HighCard extends HandType


def bestType(hand: Hand): HandType = {
    val nJokers = hand.cardCounts(Joker)

    def substituteFirstJoker(newType: Card): Hand = {
        val newCards = hand.cards.updated(
            hand.cards.indexOf(Joker), newType
        )
        new Hand(newCards, hand.bid)
    }
    
    if ((nJokers == 5) || (nJokers == 4)) FiveOfAKind // Not necessary, but accelerates it significantly
    else if (hand.cardCounts(Joker) == 0) hand.naiveHandType
    else {
        (for (cardType <- Card.types.filter(_ != Joker)) yield {
            bestType(substituteFirstJoker(cardType))
        }).max
    }
}

case class Hand(cards: Array[Card], bid: Int) extends Ordered[Hand] {
    val cardCounts: Map[Card, Int] = 
        (for (cardType <- Card.types) yield
            cardType -> cards.count(_ == cardType)
        ).toMap

    val naiveHandType: HandType = {
        cardCounts.removed(Joker) match {
            case counts if (counts.valuesIterator.contains(5)) => FiveOfAKind
            case counts if counts.valuesIterator.contains(4) => FourOfAKind
            case counts if (counts.valuesIterator.contains(3) && counts.valuesIterator.contains(2)) => FullHouse
            case counts if (counts.valuesIterator.contains(3)) => ThreeOfAKind
            case counts if (counts.valuesIterator.count(_ == 2) == 2) => TwoPairs
            case counts if (counts.valuesIterator.contains(2)) => OnePair
            case _ => HighCard
        }
    }

    val handType: HandType = bestType(this)

    def compare(that: Hand): Int = {
        val handComparison = this.handType.compare(that.handType)
        if (handComparison != 0) handComparison
        else {
            val thisCardsTuple = (cards(0), cards(1), cards(2), cards(3), cards(4))
            val thatCardsTuple = (that.cards(0), that.cards(1), that.cards(2), that.cards(3), that.cards(4))
            thisCardsTuple.compare(thatCardsTuple)
        }
    }
    
    override def toString(): String = {
        cards.mkString("") + " " + bid.toString()
    }
}

object Hand {
    def parseCards(cardsPart: String): Array[Card] = {
        Array(
            Card.parse(cardsPart.charAt(0)),
            Card.parse(cardsPart.charAt(1)),
            Card.parse(cardsPart.charAt(2)),
            Card.parse(cardsPart.charAt(3)),
            Card.parse(cardsPart.charAt(4)),
        )
    }

    def parseLine(line: String): Hand = {
        val lineParts =  line.split("\\s+")
        val cardsPart = lineParts(0)
        val bid = lineParts(1).toInt
        new Hand(parseCards(cardsPart), bid)
    }
}

val hands: Array[Hand] = io.Source.stdin.getLines().map(Hand.parseLine).toArray
quickSort(hands)
val result = hands.zipWithIndex.map(p => (p._2 + 1) * p._1.bid).sum
println(result)