case class Card(number: Int, winning: Set[Int], have: Set[Int]) {
    override def toString(): String = s"Card ${this.number}. Winning numbers: ${winning}, Having numbers: ${have}"

    def nWinning: Int = winning.intersect(have).size
    def nPoints: Int = {
        val n = this.nWinning
        if (n == 0) 0
        else (1 << (n-1))
    }
}

object Card {
    def parse(line: String): Card = {
        val splitted = line.split("\\s*:\\s+")
        val title = splitted(0)
        val numbers = splitted(1)

        val cardNumber = title.split("\\s+")(1).toInt
        
        val splittedNumbers = numbers.split("\\s+\\|\\s+")
        val winningString = splittedNumbers(0)
        val haveString = splittedNumbers(1)

        new Card(
            cardNumber,
            winningString.split("\\s+").map(_.toInt).toSet,
            haveString.split("\\s+").map(_.toInt).toSet
        )
    }
}

val result = io.Source.stdin.getLines().map(Card.parse).map(_.nPoints).sum
println(result)