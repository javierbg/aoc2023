import javax.management.RuntimeErrorException
case class Node(name: String)

sealed trait Move
case object Left  extends Move
case object Right extends Move

object Move {
    def parsMoves(line: String): Seq[Move] = {
        line.map(c => {
            if (c == 'L') Left
            else if (c == 'R') Right
            else throw new RuntimeException()
        })
    }
}

case class Network(paths: Map[Node, (Node, Node)]) {
    def minMoves(source: Node, destination: Node, moveSeq: Seq[Move]): Int = {
        def _minMoves(nMoves: Int, currentPosition: Node): Int = {
            if (currentPosition == destination) nMoves
            else {
                val nextMove = moveSeq(nMoves % moveSeq.length)
                val newPosition =
                    if (nextMove == Left) paths(currentPosition)._1
                    else paths(currentPosition)._2
                _minMoves(nMoves + 1, newPosition)
            }
        }
        _minMoves(0, source)
    }
}

object Network {
    def parse(lines: Iterator[String]): Network  = {
        new Network((for (line <- lines) yield {
            val lineParts = line.split(" = ")
            val position = new Node(lineParts(0))
            val moves = lineParts(1).substring(1, lineParts(1).length - 1).split(", ")
            position -> (new Node(moves(0)), new Node(moves(1)))
        }).toMap)
    }
}

def parse(lines: Iterator[String]): (Seq[Move], Network) = {
    val moves = Move.parsMoves(lines.next())
    lines.next()
    val network = Network.parse(lines)
    (moves, network)
}

val (moves, netowrk) = parse(io.Source.stdin.getLines())
val result = netowrk.minMoves(
    new Node("AAA"),
    new Node("ZZZ"),
    moves
)
println(result)