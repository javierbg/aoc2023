import scala.util.Random

type Vertex = Set[String]
type Edge = Set[Vertex]

val random = new Random

def popEdge(idx: Int, edges: List[Edge]): (Edge, List[Edge]) = {
    val e = edges(idx)
    (e, edges.filter(_ != e))
}

def parse(lines: Iterator[String]): List[Edge] = {
    (for { line <- lines } yield {
        val lineParts = line.split(": ")
        val fromPart = lineParts(0)
        val toParts = lineParts(1).split("\\s+")
        toParts.map(otherPart => Set(Set(fromPart), Set(otherPart))).toList
    }).flatten.toList.distinct
}

def contractRandom(vertices: Set[Vertex], edges: List[Edge]): (Set[Vertex], List[Edge]) = {
    val selectedEdgeIndex = random.between(0, edges.length)
    val (selectedEdge, restEdges) = popEdge(selectedEdgeIndex, edges)

    assert(selectedEdge.size == 2)
    val u = selectedEdge.head
    val v = selectedEdge.tail.head
    val newVertex = u union v

    val newVertices = vertices - u - v + newVertex

    val substitutions: List[(Edge, Edge)] = (for { otherEdge <- restEdges } yield {
        assert(otherEdge.size == 2)
        val x = otherEdge.head
        val y = otherEdge.tail.head

        val edgeContainsX = selectedEdge.contains(x)
        val edgeContainsY = selectedEdge.contains(y)

        if (edgeContainsX && !edgeContainsY) Some( (otherEdge, Set(newVertex, y)) )
        else if (!edgeContainsX && edgeContainsY) Some( (otherEdge, Set(x, newVertex)) )
        else None
    }).flatten

    val newEdges: List[Edge] = substitutions.foldLeft(restEdges)((acc, sub) => {
        val (toRemove, toAdd) = sub
        toAdd :: acc.filter(_ != toRemove)
    })

    (newVertices, newEdges)
}

def karger(edges: List[Edge], targetCutSize: Int): (Vertex, Vertex) = {
    val vertices: Set[Vertex] = edges.foldLeft(Set.empty[Vertex])((acc, edge) => {
        acc union edge
    })

    def _contract(remainingVertices: Set[Vertex], remainingEdges: List[Edge]): (Set[Vertex], List[Edge]) = {
        if (remainingVertices.size <= 2) (remainingVertices, remainingEdges)
        else {
            val (newRemainingVertices, newRemainingEdges) = contractRandom(remainingVertices, remainingEdges)
            _contract(newRemainingVertices, newRemainingEdges)
        }
    }

    def roll: Option[Set[Vertex]] = {
        val (finalVertices, finalEdges) = _contract(vertices, edges)
        if (finalEdges.length == targetCutSize) Some(finalVertices)
        else None
    }
    
    val x = LazyList.from(0).map(_ => roll).flatten.head
    assert(x.size == 2)
    (x.head, x.tail.head)
}

val edges = parse(io.Source.stdin.getLines())
val (partition1, partition2) = karger(edges, targetCutSize = 3)
println(partition1)
println(partition2)
val result = partition1.size * partition2.size
println(result)