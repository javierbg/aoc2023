sealed trait Direction {
    val movement: (Int, Int)
    def move(pos: (Int, Int)): (Int, Int) =
        (pos._1 + movement._1, pos._2 + movement._2)
}
object Left extends Direction {
    override val movement: (Int, Int) = (0, -1)
    override def toString(): String = "Left"
}
object Right extends Direction{
    override val movement: (Int, Int) = (0, 1)
    override def toString(): String = "Right"
}
object Up extends Direction{
    override val movement: (Int, Int) = (-1, 0)
    override def toString(): String = "Up"
}
object Down extends Direction{
    override val movement: (Int, Int) = (1, 0)
    override def toString(): String = "Down"
}

object Direction{
    val allDirections: List[Direction] = List(Left, Right, Up, Down)
}

sealed trait Tile
object EmptyTile extends Tile
case class SlopedTile(direction: Direction) extends Tile
object WallTile extends Tile

object Tile {
    def parse(c: Char): Tile = {
        c match {
            case '.' | '<' | '>' | '^' | 'v' => EmptyTile
            case '#' => WallTile
            case _ => sys.error("Invalid character")
        }
    }
}

type Graph = Map[(Int, Int), List[((Int, Int), Int)]]

def parse(lines: Iterator[String]): Array[Array[Tile]] = {
    lines.map(
        line => line.map(Tile.parse).toArray
    ).toArray ensuring(lines => lines.forall(line => line.length == lines(0).length))
}

def buildGraph(tiles: Array[Array[Tile]]): Graph = {
    val nRows = tiles.length
    val nCols = tiles(0).length

    def insideMap(pos: (Int, Int)) =
        (pos._1 >= 0) && (pos._1 < nRows) && (pos._2 >= 0) && (pos._2 < nCols)

    (for {
        (row, i) <- tiles.zipWithIndex
        (tile, j) <- row.zipWithIndex
    } yield {
        tile match {
            case WallTile => None
            case SlopedTile(direction) => {
                val neighbours = List(direction).map(_.move((i, j)))
                    .filter(pn => {
                        val (ni, nj) = pn
                        insideMap(pn) && (tiles(ni)(nj) != WallTile)
                    }).map((_, 1))
                Some((i, j) -> neighbours)
            }
            case EmptyTile => {
                val neighbours = Direction.allDirections.map(_.move((i, j)))
                    .filter(pn => {
                        val (ni, nj) = pn
                        insideMap(pn) && (tiles(ni)(nj) != WallTile)
                    }).map((_, 1))
                Some((i, j) -> neighbours)
            }
        }
    }).flatten.toMap
}

def simplifyGraph(graph: Graph, source: (Int, Int), destination: (Int, Int)): Graph = {
    val nodesToConserve = graph.filter({ case (node, neighbours) => 
        (node == source) || (node == destination) || (neighbours.length > 2)
    }).keys.toSet

    (for { node <- nodesToConserve } yield {
        val newNeighbours: List[((Int, Int), Int)] = graph(node).map({ case (oldNeighbour, _) =>
            def followPath(current: (Int, Int), last: (Int, Int), length: Int): ((Int, Int), Int) = {
                if (nodesToConserve.contains(current)) (current, length)
                else {
                    val (nextNode, edgeWeight) = graph(current).filter({case (n, _) => n != last}).head
                    followPath(nextNode, current, length + edgeWeight)
                }
            }
            followPath(oldNeighbour, node, 1)
        })
        node -> newNeighbours
    }).toMap
}

def longestPath(graph: Graph, source: (Int, Int), destination: (Int, Int)): Int = {
    def _longestPath(lenghtSoFar: Int, current: (Int, Int), traversed: Set[(Int, Int)]): Int = {
        if (current == destination) lenghtSoFar
        else graph(current).filter(p => !traversed.contains(p._1)).map({case (n, w) =>
            _longestPath(lenghtSoFar + w, n, traversed + current)
        }).maxOption.getOrElse(0)
    }
    _longestPath(0, source, Set.empty[(Int, Int)])
}


def graphviz(graph: Graph, directed: Boolean = true): String = {
    val edge = if (directed) "->" else "--"
    val tab = "    "
    val graphTitle = if (directed) "digraph G" else "graph G"

    graphTitle + "{\n" +
    graph.map({ case (node, children) =>
        def name(pos: (Int, Int)) = s"r${pos._1}c${pos._2}"
        if (children.isEmpty) s"${tab}${name(node)};"
        else {
            children.map({case (c, w) => {
                s"${tab}${name(node)} ${edge} ${name(c)} [label=${w}];"
            }}).mkString("\n")
        }
    }).mkString("\n") + "\n}"
}

val tiles = parse(io.Source.stdin.getLines())

val initialCol = tiles(0).indexWhere(_ != WallTile)
val initialPos = (0, initialCol)

val targetCol = tiles(tiles.length - 1).indexWhere(_ != WallTile)
val targetPos = (tiles.length - 1, targetCol)

val graph = buildGraph(tiles)
//println(graphviz(graph, directed = false))


val simplifiedGraph = simplifyGraph(graph, initialPos, targetPos)
// println(graphviz(simplifiedGraph, directed = false))

val result = longestPath(simplifiedGraph, initialPos, targetPos)
println(result)