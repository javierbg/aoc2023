class SkyMap(image: Array[Array[Boolean]]) {
    require(image.forall(row => row.length == image(0).length))

    val nRows = image.length
    val nCols = image(0).length

    val rowEmpty = image.map(row => !row.exists(identity))
    val colEmpty = image.transpose.map(col => !col.exists(identity))

    val galaxies: List[(Int, Int)] = {
        (for {
            (row, i) <- image.zipWithIndex
            (isGalaxy, j) <- row.zipWithIndex
        } yield {
            if (isGalaxy) Some((i, j))
            else None
        }).flatten.toList
    }

    def distanceBetweenGalaxies(pos1: (Int, Int), pos2: (Int, Int)): Int = {
        val distanceWithoutExpansion = (pos1._1 - pos2._1).abs + (pos1._2 - pos2._2).abs
        val startRow = pos1._1.min(pos2._1)
        val endRow = pos1._1.max(pos2._1)
        val startCol = pos1._2.min(pos2._2)
        val endCol = pos1._2.max(pos2._2)
        val rowExpansion = rowEmpty.slice(startRow+1, endRow).map(if (_) 1 else 0).sum
        val colExpansion = colEmpty.slice(startCol+1, endCol).map(if (_) 1 else 0).sum
        
        distanceWithoutExpansion + rowExpansion + colExpansion
    }

    def totalDistanceBetweenGalaxies: Int = {
        def _accumulateDistance(acc: Int, galaxy: (Int, Int), rest: List[(Int, Int)]): Int = {
            if (rest.isEmpty) acc
            else _accumulateDistance(
                acc + (for {otherGalaxy <- rest} yield distanceBetweenGalaxies(galaxy, otherGalaxy)).sum,
                rest.head,
                rest.tail
            )
        }
        _accumulateDistance(0, galaxies.head, galaxies.tail)
    }

    override def toString(): String = {
        image.map(row => {
            row.map(isGalaxy =>
                if (isGalaxy) '#'
                else '.'
            ).mkString("")
        }).mkString("\n")
    }
}

object SkyMap {
    def parse(lines: Iterator[String]): SkyMap = {
        new SkyMap(
            lines.map(line => line.map(_ == '#').toArray).toArray
        )
    }
}

val skyMap = SkyMap.parse(io.Source.stdin.getLines())
val result = skyMap.totalDistanceBetweenGalaxies
println(result)