import scala.collection.immutable.Queue

case class Card(number: Int, winning: Set[Int], have: Set[Int]) {
    override def toString(): String = s"Card ${this.number}. Winning numbers: ${winning}, Having numbers: ${have}"

    def nWinning: Int = winning.intersect(have).size
}

object Card {
    def parse(line: String): Card = {
        val splitted = line.split("\\s*:\\s+")
        val title = splitted(0)
        val numbers = splitted(1)

        val cardNumber = title.split("\\s+")(1).toInt
        
        val splittedNumbers = numbers.split("\\s+\\|\\s+")
        val winningString = splittedNumbers(0)
        val haveString = splittedNumbers(1)

        new Card(
            cardNumber,
            winningString.split("\\s+").map(_.toInt).toSet,
            haveString.split("\\s+").map(_.toInt).toSet
        )
    }
}

def accumulateCards(accCards: Int, nextCopies: Queue[Int], remainingCards: List[Card]): Int = {
    val topCard = remainingCards.head
    println(s"Processing card ${topCard.number}")
    val (currentBonus, nextCopies1) = nextCopies.dequeueOption.getOrElse((0, Queue.empty))
    val currentCopies = 1 + currentBonus
    println(s"I have ${currentCopies} copy(ies) of this card")
    val nWins = topCard.nWinning
    println(s"This card has ${nWins} winning numbers")

    val nextCopies2 = for ((before, toAdd) <- nextCopies1.zipAll(List.fill(nWins)(currentCopies), 0, 0)) yield before + toAdd
    println(nextCopies2)

    val newAcc = accCards + currentCopies
    val newRemainingCards = remainingCards.tail

    if (newRemainingCards.isEmpty) newAcc
    else accumulateCards(newAcc, nextCopies2, newRemainingCards)
}

val cardList = io.Source.stdin.getLines().map(Card.parse).toList
val result = accumulateCards(0, Queue[Int](), cardList)
println(result)
