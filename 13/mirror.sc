sealed trait Symmetry
case class HorizontalSymmetry(cols: Int) extends Symmetry
case class VerticalSymmetry(rows: Int) extends Symmetry

case class Board(lines: Array[Array[Boolean]]) {
    require(lines.forall(line => line.length == lines(0).length))

    val nRows = lines.length
    val nCols = lines(0).length

    def transpose: Board = {
        new Board(lines.transpose)
    }

    def checkHorizontalSymmetry: Option[Int] = {
        def _checkHorizontalSymmetry(next: Int): Option[Int] = {
            if (next >= nCols) None
            else if (checkHorizontalSymmetry(next)) Some(next)
            else _checkHorizontalSymmetry(next+1)
        }
        _checkHorizontalSymmetry(1)
    }

    def checkHorizontalSymmetry(shift: Int): Boolean = {
        lines.forall(line =>{
            val (left, right) = line.splitAt(shift)
            left.reverse.zip(right).forall(p => p._1 == p._2)
        })
    }

    def checkVerticalSymmetry: Option[Int] = transpose.checkHorizontalSymmetry

    def checkSymmetry: Symmetry = {
        checkHorizontalSymmetry.map(HorizontalSymmetry(_)).getOrElse(
            checkVerticalSymmetry.map(VerticalSymmetry(_)).get
        )
    }

    override def toString(): String = {
        lines.map(line => line.map({e =>
            if (e) "#" else "."
        }).mkString("")).mkString("\n") + "\n"
    }
}

object Board {
    def parse(lines: Iterator[String]): Board = {
        new Board(lines.takeWhile(!_.isEmpty).map(line => {
            line.map(c => {
                c match {
                    case '#' => true
                    case '.' => false
                    case _ => sys.error(s"Invalid character '${c}'")
                }
            }).toArray
        }).toArray)
    }
}

def parseAll(lines: Iterator[String]): List[Board] = {
    def _parseAll(acc: List[Board]): List[Board] = {
        if (!lines.hasNext) acc.reverse
        else _parseAll(Board.parse(lines) :: acc)
    }
    _parseAll(Nil)
}

def summarize(symmetries: List[Symmetry]): Int = {
    symmetries.map(sym => {
        sym match {
            case HorizontalSymmetry(cols) => cols
            case VerticalSymmetry(rows) => rows*100
        }
    }).sum
}

val boards = parseAll(io.Source.stdin.getLines())
val symmetries = boards.map(_.checkSymmetry)
val result = summarize(symmetries)
println(result)