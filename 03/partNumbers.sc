sealed trait EnginePart {
    override def toString(): String = {
        this match {
            case Empty => "."
            case Digit(value) => value.toString
            case Part(value) => value.toString
        }
    }

    def isDigit: Boolean = {
        this match {
            case Digit(value) => true
            case _ => false
        }
    }

    def digitValue: Option[Int] = {
        this match {
            case Digit(value) => Some(value)
            case _ => None 
        }
    }

    def isPart: Boolean = {
        this match {
            case Part(value) => true
            case _ => false
        }
    }
}

case object Empty extends EnginePart
case class Digit(value: Int) extends EnginePart
case class Part(value: Char) extends EnginePart

object EnginePart {
    def parse(c: Char): EnginePart = {
        c match {
            case '.' => Empty
            case x if x.isDigit => Digit(x.toInt - 48)
            case _ => Part(c)
        }
    }
}

class EngineSchematic(matrix: Array[Array[EnginePart]]) extends Iterable[Array[EnginePart]] {
    require(matrix.map(line => line.length == matrix(0).length).forall(identity))
    
    private val m = matrix

    override def toString(): String = {
        m.map(line => {
            line.map(_.toString).mkString("")
        }).mkString("\n")
    }

    def nRows(): Int = m.length
    def nCols(): Int = if (m.length == 0) 0 else m(0).length

    def apply(i: Int): Array[EnginePart] = {
        m(i)
    }

    override def iterator: Iterator[Array[EnginePart]] = m.iterator
}

def parseInput(lines: Iterator[String]): EngineSchematic = {
    new EngineSchematic(
        (for (line <- lines) yield line.map(EnginePart.parse).toArray).toArray
    )
}

case class PartNumber(number: Int, row: Int, startCol: Int, endCol: Int) {
    def doesNumberTouchPart(sch: EngineSchematic): Boolean = {
        ((startCol > 0)         && sch(row)(startCol - 1).isPart) ||
        ((endCol < sch.nCols()) && sch(row)(endCol).isPart      ) ||
        ((row > 0)               && sch(row-1).slice(startCol, endCol).find(_.isPart).isDefined) ||
        ((row < (sch.nRows()-1)) && sch(row+1).slice(startCol, endCol).find(_.isPart).isDefined) ||
        (((startCol > 0) && (row > 0))               && sch(row-1)(startCol-1).isPart) ||
        (((startCol > 0) && (row < (sch.nRows()-1))) && sch(row+1)(startCol-1).isPart) ||
        (((endCol < sch.nCols()) && (row > 0))               && sch(row-1)(endCol).isPart) ||
        (((endCol < sch.nCols()) && (row < (sch.nRows()-1))) && sch(row+1)(endCol).isPart)
    }
}

def findAllNumbers(engineSchematic: EngineSchematic): List[PartNumber] = {
    def digitsToNumber(digits: Array[Int]): Int = {
        digits.foldLeft(0)((acc, next) => 10*acc + next)
    }

    def getNumbersInRow(remaining: Array[EnginePart], offset: Int, row: Int, accumulated: List[PartNumber]): List[PartNumber] = {
        val nSkipped = remaining.takeWhile(!_.isDigit).length
        val possibleNext = remaining.dropWhile(!_.isDigit)
        
        if (possibleNext.isEmpty)
            accumulated 
        else {
            val digitRun = possibleNext.takeWhile(_.isDigit)
            val nextNumber: Int = digitsToNumber(digitRun.map(_.digitValue.get))
            val newAccumulated = (new PartNumber(
                    nextNumber,
                    row,
                    offset + nSkipped,
                    offset + nSkipped + digitRun.length
                )) :: accumulated
            getNumbersInRow(possibleNext.dropWhile(_.isDigit), offset + nSkipped + digitRun.length, row, newAccumulated)
        }
    }

    def getNumbersInSchematic(it: Iterator[Array[EnginePart]], accumulated: List[PartNumber], nextRow: Int): List[PartNumber] = {
        if (it.hasNext) {
            val withFirstRow = getNumbersInRow(it.next(), 0, nextRow, accumulated)
            getNumbersInSchematic(it, withFirstRow, nextRow + 1)
        } else
            accumulated
    }

    getNumbersInSchematic(engineSchematic.iterator, Nil, 0)
}


val schematic = parseInput(io.Source.stdin.getLines())
val numbers = findAllNumbers(schematic)
val result = numbers.filter(_.doesNumberTouchPart(schematic)).map(_.number).sum
println(result)