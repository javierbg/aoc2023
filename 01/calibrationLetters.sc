val numbers = Array("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine")

def startsWithNumber(s: String): Option[Int] = {
    numbers.zipWithIndex.find(pair => s.startsWith(pair._1) || s.startsWith(pair._2.toString)).map(_._2)
}

def codedNumbers(line: String): IndexedSeq[Int] = {
    (0 to line.length).map(windowStart => {
        startsWithNumber(line.substring(windowStart))
    }).flatten
}

val result: Int = io.Source.stdin.getLines()
    .map(codedNumbers)
    .map(coded => {
        (coded(0) * 10) + coded(coded.length - 1)
    }).sum

println(result)