sealed trait PulseModule {
    val name: String
    val outputModules: List[String]
}

case class FlipFlopModule(name: String, outputModules: List[String]) extends PulseModule

case class ConjuctionModule(name: String, inputModules: List[String], outputModules: List[String]) extends PulseModule

case class BroadcastModule(name: String, outputModules: List[String]) extends PulseModule

sealed trait ModuleType
object FlipFlop extends ModuleType
object Conjunction extends ModuleType
object Broadcast extends ModuleType

type Network = Map[String, PulseModule]

def parse(lines: Iterator[String]): Network = {
    val modulesPrototypes: Map[String, (ModuleType, List[String])] =
        (for { line <- lines } yield {
            val parts = line.split(" -> ")
            val lhs = parts(0)
            val rhs = parts(1)
            val (moduleType, moduleName) = lhs.charAt(0) match {
                case '%' => (FlipFlop, lhs.drop(1))
                case '&' => (Conjunction, lhs.drop(1))
                case _ => {
                    assert(lhs == "broadcaster")
                    (Broadcast, lhs)
                }
            }
            val outputs = rhs.split(", ").toList

            moduleName -> (moduleType, outputs)
        }).toMap

    (for { (moduleName, (moduleType, outputs)) <- modulesPrototypes } yield {
        val module: PulseModule = moduleType match {
            case FlipFlop => new FlipFlopModule(moduleName, outputs)
            case Conjunction => {
                val connectedModules: List[String] =
                    modulesPrototypes.filter(p => {
                        val (_, (_, otherOutputs)) = p
                        otherOutputs.contains(moduleName)
                    }).map(p => {
                        val (otherName, (_, _)) = p
                        otherName
                    }).toList

                new ConjuctionModule(moduleName, connectedModules, outputs)
            }
            case Broadcast => new BroadcastModule(moduleName, outputs)
        }

        moduleName -> module
    }).toMap
}

def interpretCounters(network: Network): Array[Int] = {
    val lowestBitModules = network("broadcaster").outputModules

    def restBits(acc: List[PulseModule], lastBitModule: PulseModule): List[PulseModule] = {
        val connectedFlipFlops = lastBitModule.outputModules.filterNot(m => network(m).isInstanceOf[ConjuctionModule])
        if (connectedFlipFlops.isEmpty) (lastBitModule :: acc).reverse
        else if (connectedFlipFlops.length == 1) {
            val nextBitModule = network(connectedFlipFlops(0))
            restBits(lastBitModule :: acc, nextBitModule)
        } else sys.error("")
    }

    val hubs: Array[String] = lowestBitModules.map(lbm => {
        val connectedConjunctions = network(lbm).outputModules.filter(m => network(m).isInstanceOf[ConjuctionModule])
        assert(connectedConjunctions.length == 1)
        connectedConjunctions.head
    }).toArray

    val counterBits: Array[Array[PulseModule]] = lowestBitModules.map(lbm => restBits(Nil, network(lbm)).toArray).toArray

    counterBits.zip(hubs).map(p => {
        val (bits, hub) = p

        (for {(bit, pos) <- bits.zipWithIndex} yield {
            if (bit.outputModules.contains(hub)) 1 << pos
            else 0
        }).sum
    })
}

def gcd(a: Long, b: Long): Long = {
    if (b == 0) a else gcd(b, a % b)
}

def lcm(ns: Iterable[Long]): Long = {
    def _lcm(a: Long, b: Long): Long = a * (b / gcd(a, b))
    ns.foldLeft(1L)(_lcm)
}

val network = parse(io.Source.stdin.getLines())
val modulos = interpretCounters(network)
println(modulos.mkString(", "))

val result = lcm(modulos.map(_.toLong))
println(result)