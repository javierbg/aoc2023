import numpy as np
from fractions import Fraction
from itertools import product
from scipy.optimize import linprog
from tqdm import tqdm
from sympy import Symbol, Eq, solve
from typing import Optional, Iterable

Hail = tuple[tuple[int, int, int], tuple[int, int, int]]
ZERO = Fraction(0, 1)

def main():
    hails: list[Hail] = list()
    while True:
        try:
            line = input()
            pos_str, vel_str = line.split("@")
            pos = tuple(map(lambda s: int(s.strip()), pos_str.split(",")))
            vel = tuple(map(lambda s: int(s.strip()), vel_str.split(",")))
            hails.append((pos, vel))
        except EOFError:
            break

    (x01, y01, z01), (vx1, vy1, vz1) = hails[0]
    (x02, y02, z02), (vx2, vy2, vz2) = hails[1]

    # The differences between the two hails' positions at times t1 and t2
    # are related such that:
    #      (x01 + vx1 * t1) - (x02 + vx2 * t2) = (x0 + vx * t1) - (x0 + vx * t2)
    #      (y01 + vy1 * t1) - (y02 + vy2 * t2) = (y0 + vy * t1) - (y0 + vy * t2)
    # Use these two equations to solve for t1 and t2 using sympy

    s = {n: Symbol(n) for n in [
        'x01', 'vx1', 'x02', 'vx2',
        'y01', 'vy1', 'y02', 'vy2',
        't1', 't2', 'vx', 'vy'
    ]}

    equations = [
        Eq((s['x01'] + s['vx1'] * s['t1']) - (s['x02'] + s['vx2'] * s['t2']), (s['t1'] - s['t2']) * s['vx']),
        Eq((s['y01'] + s['vy1'] * s['t1']) - (s['y02'] + s['vy2'] * s['t2']), (s['t1'] - s['t2']) * s['vy']),
    ]

    solutions = solve(equations, [s['t1'], s['t2']])

    # Print the expressions so that I can use them in the Scala version :P

    # To be fair, I tried deriving them using pen and paper, but there are
    # so many symbols that I must have messed up midway through and I was
    # getting wrong solutions!
    print(solutions)

    t1_expression = solutions[s['t1']]
    t2_expression = solutions[s['t2']]
    
    def compute_t1_t2(vx: int, vy: int) -> Optional[tuple[Fraction, Fraction]]:
        t1_val = t1_expression.subs([
            (s['x01'], x01),
            (s['vx1'], vx1),
            (s['x02'], x02),
            (s['vx2'], vx2),

            (s['y01'], y01),
            (s['vy1'], vy1),
            (s['y02'], y02),
            (s['vy2'], vy2),

            (s['vx'], vx),
            (s['vy'], vy),
        ])

        t2_val = t2_expression.subs([
            (s['x01'], x01),
            (s['vx1'], vx1),
            (s['x02'], x02),
            (s['vx2'], vx2),

            (s['y01'], y01),
            (s['vy1'], vy1),
            (s['y02'], y02),
            (s['vy2'], vy2),

            (s['vx'], vx),
            (s['vy'], vy),
        ])

        # The obtained value must exist and be positive
        # (no time travel)
        if (not t1_val.is_rational) or (not t2_val.is_rational):
            return None

        if (t1_val < 0) or (t2_val < 0):
            return None

        return (
            Fraction(t1_val.numerator, t1_val.denominator),
            Fraction(t2_val.numerator, t2_val.denominator),
        )
    
    def compute_vz(t1: Fraction, t2: Fraction) -> Fraction:
        num = (z01 + vz1 * t1) - (z02 + vz2 * t2)
        den = t1 - t2
        return num / den
    
    def compute_x0(t1: Fraction, vx: int) -> Fraction:
        return x01 + (vx1 - vx) * t1
    
    def compute_y0(t1: Fraction, vy: int) -> Fraction:
        return y01 + (vy1 - vy) * t1
    
    def compute_z0(t1: Fraction, vz: int) -> Fraction:
        return z01 + (vz1 - vz) * t1
    
    def spiral() -> Iterable[tuple[int, int]]:
        # Generates a spiral centered at the origin

        # This is an infinite generator that returns
        # coordinate pairs making a spiral.
        di = 1
        dj = 0
        segment_length = 1
        i = 0
        j = 0
        segment_passed = 0

        yield (i, j)

        while True:
            i += di
            j += dj
            segment_passed += 1
            yield (i, j)

            if segment_passed == segment_length:
                segment_passed = 0
                di, dj = -dj, di

                if dj == 0:
                    segment_length += 1
    
    for vx, vy in spiral():
        if (result := compute_t1_t2(vx, vy)) is None:
            continue
        
        t1, t2 = result

        # All components of the velocity and initial
        # position must be integers.
        vz = compute_vz(t1, t2)

        if vz.denominator != 1:
            continue
        else:
            vz = vz.numerator
        x0 = compute_x0(t1, vx)
        y0 = compute_y0(t1, vy)
        z0 = compute_z0(t1, vz)

        if (x0.denominator != 1) or (y0.denominator != 1) or (z0.denominator != 1):
            continue

        # The candidate trajectory (x0, y0, z0), (vx, vy, vz) must hit
        # all other hails to be a solution.
        for (x0i, y0i, z0i), (vxi, vyi, vzi) in hails[2:]:
            # If same velocity then they cannot hit, unless same initial position
            if (
                ((x0i != x0) and (vx == vxi)) or
                ((y0i != z0) and (vy == vyi)) or
                ((z0i != y0) and (vz == vzi))
            ):
                break

            # Check the time of the hit in x, y and z.
            # The trajectories hit each other if they are all equal
            tx = ZERO if (x0i == x0) else ((x0i - x0) / (vx - vxi))
            ty = ZERO if (y0i == y0) else ((y0i - y0) / (vy - vyi))

            if (tx != ty): break

            tz = ZERO if (z0i == z0) else ((z0i - z0) / (vz - vzi))

            if (ty != tz): break
        else:
            # Got through all trajectories without breaking, which means
            # this is a valid solution
            print("solution found!")
            print(f"x0={x0.numerator}, y0={y0.numerator}, z0={z0.numerator}")
            print(f"vx={vx}, vy={vy}, z0={vz}")

            print(x0.numerator + y0.numerator + z0.numerator)
            break
    


if __name__ == '__main__':
    main()
