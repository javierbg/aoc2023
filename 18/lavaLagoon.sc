sealed trait Direction {
    val movement: (Int, Int)
    def move(pos: (Int, Int), nSteps: Int = 1): (Int, Int) =
        if (nSteps == 0) pos
        else move((pos._1 + movement._1, pos._2 + movement._2), nSteps-1)
}
object Left extends Direction {
    override val movement: (Int, Int) = (0, -1)
    override def toString(): String = "Left"
}
object Right extends Direction{
    override val movement: (Int, Int) = (0, 1)
    override def toString(): String = "Right"
}
object Up extends Direction{
    override val movement: (Int, Int) = (-1, 0)
    override def toString(): String = "Up"
}
object Down extends Direction{
    override val movement: (Int, Int) = (1, 0)
    override def toString(): String = "Down"
}

object Direction {
    def parse(c: Char): Direction = c match {
        case 'U' => Up
        case 'D' => Down
        case 'L' => Left
        case 'R' => Right
        case _ => sys.error("Invalid character")
    }
}

case class Move(direction: Direction, distance: Int)

case class DigPlan(moves: List[Move]) {
    def carryOut: List[(Int, Int)] = {
        def _caryyOut(acc: List[(Int, Int)], remainingMoves: List[Move]): List[(Int, Int)] = {
            if (remainingMoves.isEmpty) acc
            else {
                val move = remainingMoves.head
                if (move.distance <= 0) _caryyOut(acc, remainingMoves.tail)
                else {
                    val currentPos = acc.head
                    val newPosition = move.direction.move(currentPos)
                    val newHeadMove = new Move(move.direction, move.distance - 1)
                    _caryyOut(newPosition :: acc, newHeadMove :: remainingMoves.tail)
                }
            }
        }

        _caryyOut(List((0, 0)), moves)
    }
}

object DigPlan {
    def parse(lines: Iterator[String]): DigPlan = {
        new DigPlan(lines.map(line => {
            val parts = line.split("\\s+")
            val directionString = parts(0)
            assert(directionString.length() == 1)
            val direction = Direction.parse(directionString.charAt(0))
            val distance = parts(1).toInt
            new Move(direction, distance)
        }).toList)
    }
}

def lagoonSize(trench: List[(Int, Int)]): Int = {
    // Shoelace formula
    def computeArea(acc: Int, currentPos: (Int, Int), restLoop: List[(Int, Int)]): Int = {
        if (restLoop.isEmpty) acc.abs / 2
        else {
            val nextPos = restLoop.head
            computeArea(
                acc + ((currentPos._1 * nextPos._2) - (currentPos._2 * nextPos._1)),
                nextPos,
                restLoop.tail
            )
        }
    }

    val area = computeArea(0, trench.head, trench.tail)
    val nBoundaryPoints = trench.length - 1
    assert((nBoundaryPoints % 2) == 0)
    val nInteriorPoints = area - (nBoundaryPoints / 2) + 1 // by Pick's theorem

    nBoundaryPoints + nInteriorPoints
}

val plan = DigPlan.parse(io.Source.stdin.getLines())
val trench = plan.carryOut
val result = lagoonSize(trench)
println(result)