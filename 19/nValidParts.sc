/*
Grammar:

<program> ::= <list-workflows>
<list-workflows> ::= <workflow> <list-workflows> | 0
<workflow> ::= <id> '{' <rule-list> '}'
<rule-list> ::= <rule> ',' <rule-list> | <default-rule> 
<rule> ::= <condition> ':' <action>
<condition> ::= <id> '<' <number> | <id> '>' <number>
<default-rule> ::= <action>
<action> ::= <id>

Tokens:
<id>
'<' '>'
':'
'{' '}'
<number>
','
*/

sealed trait Token {
    def isIdentifier: Boolean = false
    def isNumber: Boolean = false
}
object LessThan extends Token
object GreaterThan extends Token
object Colon extends Token
object OpenBrace extends Token
object CloseBrace extends Token
object Comma extends Token
case class Identifier(id: String) extends Token {
    override def isIdentifier: Boolean = true
}
case class Number(n: Int) extends Token {
    override def isNumber: Boolean = true
}

object AnyIdentifier extends Identifier("")
object AnyNumber extends Number(0)

def tokenize(text: String): List[Token] = {
    def _tokenize(acc: List[Token], remaining: String): List[Token] = {
        if (remaining.isEmpty) acc.reverse
        else {
            val trimmedRemaining = remaining.trim()
            val (nextToken, nConsumed) = trimmedRemaining.charAt(0) match {
                case '<' => (LessThan, 1)
                case '>' => (GreaterThan, 1)
                case ':' => (Colon, 1)
                case '{' => (OpenBrace, 1)
                case '}' => (CloseBrace, 1)
                case ',' => (Comma, 1)
                case c if c.isDigit => {
                    val numberString = trimmedRemaining.takeWhile(_.isDigit)
                    (new Number(numberString.toInt), numberString.length)
                }
                case c if c.isLetter => {
                    val idString = trimmedRemaining.takeWhile(_.isLetter)
                    (new Identifier(idString), idString.length)
                }
                case c => sys.error(s"Invalid next character: '$c'")
            }

            _tokenize(nextToken :: acc, trimmedRemaining.drop(nConsumed))
        }
    }
    _tokenize(Nil, text)
}

val EmptyRange = (0 to -1)

case class PartRange(x: Range, m: Range, a: Range, s: Range) {
    def isEmpty: Boolean = x.isEmpty || m.isEmpty || a.isEmpty || s.isEmpty

    def size: Long = x.length.toLong * m.length.toLong * a.length.toLong * s.length.toLong

    def fieldsArray: Array[Range] = Array(x, m, a ,s)

    def restrictFieldLessThan(field: String, upper: Int): (PartRange, PartRange) = {
        val fields = fieldsArray
        val fieldIndex = PartRange.fieldMap(field)
        val rangeToRestrict = fields(fieldIndex)

        val (accepted, rejected) =
            if (rangeToRestrict.contains(upper)) rangeToRestrict.splitAt(upper - rangeToRestrict.start)
            else if (upper > rangeToRestrict.end) (rangeToRestrict, EmptyRange)
            else (EmptyRange, rangeToRestrict)

        val updatedRangesAccepted = fields.updated(fieldIndex, accepted)
        val updatedRangesRejected = fields.updated(fieldIndex, rejected)

        (
            new PartRange(updatedRangesAccepted(0), updatedRangesAccepted(1), updatedRangesAccepted(2), updatedRangesAccepted(3)),
            new PartRange(updatedRangesRejected(0), updatedRangesRejected(1), updatedRangesRejected(2), updatedRangesRejected(3)),
        ) ensuring(parts => (parts._1.size + parts._2.size) == size)
    }

    def restrictFieldGreaterThan(field: String, lower: Int): (PartRange, PartRange) = {
        val fields = fieldsArray
        val fieldIndex = PartRange.fieldMap(field)
        val rangeToRestrict = fields(fieldIndex)

        val (rejected, accepted) =
            if (rangeToRestrict.contains(lower)) rangeToRestrict.splitAt(lower - rangeToRestrict.start + 1)
            else if (lower < rangeToRestrict.start) (EmptyRange, rangeToRestrict)
            else (rangeToRestrict, EmptyRange)

        val updatedRangesAccepted = fields.updated(fieldIndex, accepted)
        val updatedRangesRejected = fields.updated(fieldIndex, rejected)

        (
            new PartRange(updatedRangesAccepted(0), updatedRangesAccepted(1), updatedRangesAccepted(2), updatedRangesAccepted(3)),
            new PartRange(updatedRangesRejected(0), updatedRangesRejected(1), updatedRangesRejected(2), updatedRangesRejected(3)),
        ) ensuring(parts => (parts._1.size + parts._2.size) == size)
    }
}

object PartRange {
    val empty: PartRange = new PartRange(EmptyRange, EmptyRange, EmptyRange, EmptyRange)

    val fieldMap: Map[String, Int] = Map(
        "x" -> 0,
        "m" -> 1,
        "a" -> 2,
        "s" -> 3
    )
}

sealed trait Condition {
    def apply(partRange: PartRange): (PartRange, PartRange)
}
case class LessThanCondition(field: String, upper: Int) extends Condition {
    override def apply(partRange: PartRange): (PartRange, PartRange) = partRange.restrictFieldLessThan(field, upper)
}
case class GreaterThanCondition(field: String, lower: Int) extends Condition {
    override def apply(partRange: PartRange): (PartRange, PartRange) = partRange.restrictFieldGreaterThan(field, lower)
}
object AlwaysCondition extends Condition {
    override def apply(partRange: PartRange): (PartRange, PartRange) = (partRange, PartRange.empty)
}

sealed trait Action
object Accept extends Action
object Reject extends Action
case class Goto(workflowName: String) extends Action

case class Rule(condition: Condition, action: Action)
case class Workflow(rules: List[Rule])
case class Program(workflows: Map[String, Workflow]) {    
    def nPlausible(part: PartRange): Long = {
        def _restrict(accPlausible: Long, accImplausible: Long, current: PartRange, remainingRules: List[Rule]): (Long, Long) = {
            if (current.isEmpty) (accPlausible, accImplausible)
            else if (remainingRules.isEmpty) sys.error("Ran out of rules")
            else {
                val nextRule = remainingRules.head
                
                nextRule.action match {
                    case Accept => {
                        val (acceptedRange, otherRange) = nextRule.condition(current)
                        _restrict(acceptedRange.size + accPlausible, accImplausible, otherRange, remainingRules.tail)
                    }
                    case Reject => {
                        val (rejectedRange, otherRange) = nextRule.condition(current)
                        _restrict(accPlausible, rejectedRange.size + accImplausible, otherRange, remainingRules.tail)
                    }
                    case Goto(workflowName) => {
                        val (gotoRange, otherRange) = nextRule.condition(current)

                        val (newAP, newAI) = _restrict(accPlausible, accImplausible, gotoRange, workflows(workflowName).rules)
                        _restrict(newAP, newAI, otherRange, remainingRules.tail)
                    }
                }
            }
        }

        val (plausible, implausible) = _restrict(0L, 0L, part, workflows("in").rules)
        assert((plausible + implausible) == part.size)
        plausible
    }
}

def expectSymbol(token: Token, tokens: List[Token]): Option[Token] = {
    if (tokens.isEmpty) None
    else if (token == tokens.head) Some(tokens.head)
    else None
}

def expectIdentifier(tokens: List[Token]): Option[Identifier] = {
    if (tokens.isEmpty) None
    else tokens.head match {
        case id @ Identifier(_) => Some(id)
        case _ => None
    }
}

def expectNumber(tokens: List[Token]): Option[Number] = {
    if (tokens.isEmpty) None
    else tokens.head match {
        case n @ Number(_) => Some(n)
        case _ => None
    }
}

def parseProgram(tokens: List[Token]): Option[Program] = {
    parseWorkflowList(tokens).map(p => (Program(p._1), p._2))
        .map(p => {
            if (p._2.isEmpty) Some(p._1)
            else None
        }).flatten
}

def parseWorkflowList(tokens: List[Token]): Option[(Map[String, Workflow], List[Token])] = {
    def _parseWorkflowList(acc: List[(String, Workflow)], remTokens: List[Token]): Option[(Map[String, Workflow], List[Token])] = {
        if (remTokens.isEmpty) Some((acc.toMap, remTokens))
        else {
            val wf = parseWorkflow(remTokens)

            if (wf.isDefined) {
                val (wfName, workflow, rest) = wf.get
                _parseWorkflowList((wfName, workflow) :: acc, rest)
            } else None
        }
    }

    
    _parseWorkflowList(Nil, tokens)
}

def parseWorkflow(tokens: List[Token]): Option[(String, Workflow, List[Token])] = {
    expectIdentifier(tokens)
        .map(wfid => expectSymbol(OpenBrace, tokens.drop(1)).map(_ => wfid)).flatten
        .map(wfid => parseRuleList(tokens.drop(2)).map(p => (wfid, p._1, p._2))).flatten
        .map(p => expectSymbol(CloseBrace, p._3).map(_ => (p._1, p._2, p._3.tail))).flatten
        .map(p => (p._1.id, new Workflow(p._2), p._3))
}

def parseRuleList(tokens: List[Token]): Option[(List[Rule], List[Token])] = {
    def _parseRuleList(acc: List[Rule], remTokens: List[Token]): Option[(List[Rule], List[Token])] = {
        if (remTokens.isEmpty || expectIdentifier(remTokens).isEmpty) None

        val r = parseRule(remTokens)

        if (r.isDefined) {
            val (rule, rest) = r.get
            if (expectSymbol(Comma, rest).isDefined)
                _parseRuleList(rule :: acc, rest.drop(1))
            else None
        } else {
            parseDefaultRule(remTokens)
                .map(p => ((p._1 :: acc).reverse, p._2))
        }
    }

    
    _parseRuleList(Nil, tokens)
}

def parseRule(tokens: List[Token]): Option[(Rule, List[Token])] = {
    parseCondition(tokens)
        .map(p => expectSymbol(Colon, p._2).map(_ => (p._1, p._2.tail))).flatten
        .map(p => parseAction(p._2).map(p1 => (p._1, p1._1, p1._2))).flatten
        .map(p => {
            val (condition, action, rest) = p
            (new Rule(condition, action), rest)
        })
}

def parseAction(tokens: List[Token]): Option[(Action, List[Token])] = {
    expectIdentifier(tokens).map(id => {
        val action: Action = id.id match {
            case "R" => Reject
            case "A" => Accept
            case s => Goto(s)
        }

        (action, tokens.tail)
    })
}

def parseDefaultRule(tokens: List[Token]): Option[(Rule, List[Token])] = {
    parseAction(tokens).map(p => {
        val (action, rest) = p

        (new Rule(AlwaysCondition, action), rest)
    })
}

def parseCondition(tokens: List[Token]): Option[(Condition, List[Token])] = {
    expectIdentifier(tokens)
        .zip(expectSymbol(LessThan, tokens.drop(1)))
        .zip(expectNumber(tokens.drop(2)))
        .map(ts => {
            val ((id, _), n) = ts

            (new LessThanCondition(id.id, n.n), tokens.drop(3))
    }).orElse(expectIdentifier(tokens)
        .zip(expectSymbol(GreaterThan, tokens.drop(1)))
        .zip(expectNumber(tokens.drop(2)))
        .map(ts => {
            val ((id, _), n) = ts

            (new GreaterThanCondition(id.id, n.n), tokens.drop(3))
    }))
}

def parse(lines: Iterator[String]): Program = {
    val tokens = tokenize(lines.takeWhile(_ != "").mkString("\n"))
    val program = parseProgram(tokens)

    program.getOrElse(sys.error("Invalid program"))
}

val program = parse(io.Source.stdin.getLines())
val result = program.nPlausible(new PartRange(1 to 4000, 1 to 4000, 1 to 4000, 1 to 4000))
println(result)