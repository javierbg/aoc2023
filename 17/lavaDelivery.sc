sealed trait Direction {
    val movement: (Int, Int)
    def move(pos: (Int, Int), nSteps: Int = 1): (Int, Int) =
        if (nSteps == 0) pos
        else move((pos._1 + movement._1, pos._2 + movement._2), nSteps-1)
}
object Left extends Direction {
    override val movement: (Int, Int) = (0, -1)
    override def toString(): String = "Left"
}
object Right extends Direction{
    override val movement: (Int, Int) = (0, 1)
    override def toString(): String = "Right"
}
object Up extends Direction{
    override val movement: (Int, Int) = (-1, 0)
    override def toString(): String = "Up"
}
object Down extends Direction{
    override val movement: (Int, Int) = (1, 0)
    override def toString(): String = "Down"
}

object Direction{
    def fromTo(from: (Int, Int), to: (Int, Int)): Direction = {
        val (fromR, fromC) = from
        val (toR, toC) = to

        if (fromR == toR) // column movement
            if ((toC - fromC) > 0) Right
            else Left
        else if (fromC == toC) // row movement
            if ((toR - fromR) > 0) Down
            else Up
        else sys.error("Cannot move diagonally")
    }

    val allDirections: Set[Direction] = Set(Left, Right, Up, Down)
}

case class Node(connections: Map[Direction, Map[(Int, Int), Int]])

def parse(lines: Iterator[String]): Array[Array[Node]] = {
    val heatLosses = lines.map(line => line.map(c => c.toInt - 48).toArray).toArray
    assert(heatLosses.forall(row => row.length == heatLosses(0).length))
    val nRows = heatLosses.length
    val nCols = heatLosses(0).length

    def heatLoss(from: (Int, Int), to: (Int, Int)): Option[Int] = {
        val (srcR, srcC) = from
        val (dstR, dstC) = to

        val movement = Direction.fromTo(from, to)

        def _heatLoss(acc: Int, stepFrom: (Int, Int)): Int = {
            if (stepFrom == to)
                acc
            else {
                val newPos = movement.move(stepFrom)
                val hl = heatLosses(newPos._1)(newPos._2)
                _heatLoss(acc + hl, newPos)
            }
        }

        if (
            (dstR >= 0) && (dstR < nRows) &&
            (dstC >= 0) && (dstC < nCols)
        ) Some(_heatLoss(0, from))
        else None
    }

    (for {
        i <- 0 until nRows
    } yield {
        (for {
            j <- 0 until nCols
        } yield {

            val connections: Map[Direction, Map[(Int, Int), Int]] = (
                for {direction <- Direction.allDirections} yield {
                    direction -> (
                        for {nSteps <- 1 to 3} yield {
                            val toPos = direction.move((i, j), nSteps)
                            heatLoss((i, j), toPos).map(hl => toPos -> hl)
                        }
                    ).flatten.toMap
                }
            ).toMap

            new Node(connections)
        }).toArray
    }).toArray
}

def distance(p1: (Int, Int), p2: (Int, Int)): Int = (p1._1 - p2._1).abs + (p1._2 - p2._2).abs

def bestPath(nodes: Array[Array[Node]]): Int = {
    val initialPos = (0, 0)
    val (nRows, nCols) = (nodes.length, nodes(0).length)
    val targetPos = (nRows-1, nCols-1)

    val bestDistance = collection.mutable.Map[((Int, Int), Boolean), Option[Int]]().withDefault(p => None)
    bestDistance.addOne(((initialPos, true), Some(0)))
    bestDistance.addOne(((initialPos, false), Some(0)))

    val optimistEstimation = collection.mutable.Map[((Int, Int), Boolean), Option[Int]]().withDefault(p => None)
    optimistEstimation.addOne(((initialPos, true), Some(distance(initialPos, targetPos))))
    optimistEstimation.addOne(((initialPos, false), Some(distance(initialPos, targetPos))))

    val openSet = collection.mutable.HashSet[((Int, Int), Boolean)]()
    openSet.add((initialPos, true))
    openSet.add((initialPos, false))

    def popMin(): ((Int, Int), Boolean) = {
        val min = openSet.minBy(optimistEstimation(_).getOrElse(Int.MaxValue))
        openSet.remove(min)
        min
    }

    val foundTargets = collection.mutable.Set[Boolean](true, false)
    val minPaths = collection.mutable.Map[Boolean, Int]()

    def _bestPath(): Int = {
        if (openSet.isEmpty) sys.error("Didn't find a path")
        else {
            val (current, nextHorizontal) = popMin()
            val currentGScore = bestDistance((current, nextHorizontal)).get

            if (current == targetPos) {
                foundTargets.remove(nextHorizontal)
                minPaths(nextHorizontal) = currentGScore

                if (foundTargets.isEmpty) minPaths.values.min
                else _bestPath()
            }
            else {
                val possibleDirections: Set[Direction] =
                    if (nextHorizontal) Set(Left, Right)
                    else Set(Up, Down)
                
                nodes(current._1)(current._2).connections.filter(p => possibleDirections.contains(p._1)).foreach(p => {
                    val (direction, neighbours) = p
                    neighbours.foreach(q => {
                        val (neighbour, hl) = q
                        val tentativeGScore = currentGScore + hl
                        val neighbourGScore = bestDistance((neighbour, !nextHorizontal))
                        if (!neighbourGScore.isDefined || (tentativeGScore < neighbourGScore.get)) {
                            bestDistance((neighbour, !nextHorizontal)) = Some(tentativeGScore)
                            optimistEstimation((neighbour, !nextHorizontal)) = Some(tentativeGScore + distance(neighbour, targetPos))

                            if (!openSet.exists(_ == (neighbour, !nextHorizontal))) {
                                openSet.add((neighbour, !nextHorizontal))
                            }
                        }
                    })
                })
                _bestPath()
            }
        }
    }
    _bestPath()
}

val nodes = parse(io.Source.stdin.getLines())
val result = bestPath(nodes)
println(result)