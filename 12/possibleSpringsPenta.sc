val Operational = '.'
val Damaged = '#'
val Unknown = '?'


def parse(line: String): (String, List[Int]) = {
    val lineComponents = line.split("\\s+")

    val row = (for {i <- 0 until 5} yield lineComponents(0)).mkString("?")

    val reportElements = (for {i <- 0 until 5} yield lineComponents(1)).mkString(",")
    val report = reportElements.split(",").map(_.toInt).toList

    (row, report)
}

def countPossibilities(row: String, report: List[Int]): Long = {
    val memory = scala.collection.mutable.HashMap[(String, List[Int], Boolean), Long]()

    def _countPossibilities(row: String, report: List[Int], lastDamaged: Boolean): Long = {
        memory.getOrElseUpdate(
            (row, report, lastDamaged),
            
            if (row.isEmpty())
                if (report.isEmpty || (lastDamaged && (report.length == 1) && (report.head == 0))) 1
                else 0
            else {
                row.head match {
                    case Unknown =>
                        _countPossibilities(row.updated(0, Operational), report, lastDamaged) +
                        _countPossibilities(row.updated(0, Damaged), report, lastDamaged)
                    case Damaged =>
                        if (report.isEmpty) 0
                        else {
                            if (report.head > 0) _countPossibilities(row.drop(1), (report.head - 1) :: report.tail, true)
                            else 0
                        }
                    case Operational =>
                        if (lastDamaged)
                            if (report.head == 0) _countPossibilities(row.drop(1), report.tail, false)
                            else  0
                        else _countPossibilities(row.drop(1), report, false)
                        
                }
            }
        )
    }

    _countPossibilities(row, report, false)
}

val result = io.Source.stdin.getLines().map(parse).map(p =>{
    val (springRow, report) = p
    val n = countPossibilities(springRow, report)
    println(n)
    n
}).sum

println(result)