from scipy.optimize import linprog
import numpy as np

def main():
    hails = list()
    while True:
        try:
            line = input()
            pos_str, vel_str = line.split("@")
            pos = tuple(map(lambda s: int(s.strip()), pos_str.split(",")))
            vel = tuple(map(lambda s: int(s.strip()), vel_str.split(",")))
            hails.append((pos, vel))
        except EOFError:
            break

    x_min, x_max = 200000000000000, 400000000000000
    y_min, y_max = 200000000000000, 400000000000000

    acc = 0
    for hail_idx, hail in enumerate(hails):
        (x01, y01, _), (vx1, vy1, _) = hail
        for (x02, y02, _), (vx2, vy2, _) in hails[hail_idx+1:]:
            # Set up a linear program with two variables:
            # The times at which each trajectory hits the
            # intersection point: t1 and t2.

            # We are only interested in feasability, there
            # is no function to optimize:
            c = np.array([1, 1])

            # Upper bound restrictions:
            # Hit must happen inside specified region
            A_ub = np.array([
                [-vx1, 0],
                [ vx1, 0],
                [-vy1, 0],
                [ vy1, 0]
            ])
            b_ub = np.array([
                x01 - x_min,
                x_max - x01,
                y01 - y_min,
                y_max - y01
            ])

            # Equality restrictions:
            # Trajectory 1 at time t1 must be at the same
            # point as trajectory 2 at time t2
            A_eq = np.array([
                [vx1, -vx2],
                [vy1, -vy2]
            ])
            b_eq = np.array([
                x02 - x01,
                y02 - y01
            ])

            # Only consider positive values for t1 and t2
            # (intersections can only occur in the future)
            bounds = (0, None)
            result = linprog(c, A_ub, b_ub, A_eq, b_eq, bounds)
            if result.status == 0:
                acc += 1
            elif result.status != 2:
                raise RuntimeError("linprog error")
    print(acc)


if __name__ == '__main__':
    main()
