def parse(lines: Iterator[String]): List[List[Int]] = {
    lines.map(line => {
        line.split("\\s+").map(_.toInt).toList
    }).toList
}

def sequenceDiff(sequence: List[Int]): List[Int] = {
    def _sequenceDiff(remaining: List[Int], built: List[Int]): List[Int] = {
        if (remaining.tail.isEmpty) built
        else {
            val newBuilt = (remaining.tail.head - remaining.head) :: built
            _sequenceDiff(remaining.tail, newBuilt)
        }
    }

    _sequenceDiff(sequence, Nil).reverse
}

def predictSequence(seq: List[Int]): Int = {
    if (seq.forall(_ == 0)) 0
    else {
        seq.last + predictSequence(sequenceDiff(seq))
    }
}

val sequences = parse(io.Source.stdin.getLines())
val result = sequences.map(predictSequence).sum
println(result)
