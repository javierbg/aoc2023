case class Coordinate(x: Int, y: Int, z: Int) {
    def toList: List[Int] = List(x, y , z)
}

object Coordinate {
    def parse(s: String): Coordinate = {
        val parts = s.split(",").map(_.toInt)
        assert(parts.length == 3)
        new Coordinate(parts(0), parts(1), parts(2))
    }
}

class CoordIt(direction: (Int, Int, Int), begin: Coordinate, end: Coordinate) extends Iterator[Coordinate] {
    var current = begin
    var notFinished = true

    override def hasNext: Boolean = notFinished

    override def next(): Coordinate = {
        val r = current

        if (current == end) notFinished = false
        else current = new Coordinate(current.x + direction._1, current.y + direction._2, current.z + direction._3)

        r
    }
}

class Brick(_name: Char, _begin: Coordinate, _end: Coordinate, _direction: Option[(Int, Int, Int)] = None) extends Iterable[Coordinate] {
    require(
        _begin.toList.zip(_end.toList).map(p => if (p._1 == p._2) 1 else 0).sum >= 2
    )

    val name = _name
    val begin = _begin
    val end = _end

    val direction: (Int, Int, Int) = _direction.getOrElse(
        {
        val diffX = end.x - begin.x
        val diffY = end.y - begin.y
        val diffZ = end.z - begin.z

        (
            if (diffX == 0) 0 else diffX / diffX.abs,
            if (diffY == 0) 0 else diffY / diffY.abs,
            if (diffZ == 0) 0 else diffZ / diffZ.abs
        )
    })

    def dropBy(height: Int): Brick = {
        new Brick(
            name,
            new Coordinate(begin.x, begin.y, begin.z - height),
            new Coordinate(end.x, end.y, end.z - height),
            Some(direction)
        )
    }

    override def iterator: Iterator[Coordinate] = new CoordIt(direction, begin, end)

    override def toString(): String = name.toString()
        //s"${begin.x},${begin.y},${begin.z}~${end.x},${end.y},${end.z}"
}

object Brick {
    var nextName = 'A'

    def parse(line: String): Brick = {
        val parts = line.split("~")
        assert(parts.length == 2)
        val beginCoord = Coordinate.parse(parts(0))
        val endCoord = Coordinate.parse(parts(1))

        val r = new Brick(nextName, beginCoord, endCoord)
        nextName = (nextName.toInt + 1).toChar
        r
    }
}

def settle(bricks: List[Brick]): Map[Brick, Set[Brick]] = {
    val (settled, notSettled) = {
        val (settledArr, notSettledArr) =
            bricks.partition(b => (b.begin.z == 1) || (b.end.z == 1))
        (
            collection.mutable.LinkedHashSet[Brick](settledArr:_*), 
            collection.mutable.LinkedHashSet[Brick](notSettledArr:_*)
        )
    }

    val floorHeight = collection.mutable.HashMap[(Int, Int), (Int, Option[Brick])]().withDefault(_ => (0, None))
    settled.foreach(brick => {
        for { cube <- brick } {
            floorHeight.updateWith((cube.x, cube.y))(height => {
                height.map(p => {
                    val (prevHeight, prevBrick) = p
                    if (cube.z >  prevHeight) (cube.z, Some(brick))
                    else p
                }).orElse(Some((cube.z, Some(brick))))
            })
        }
    })

    val brickSupportedBy = collection.mutable.HashMap[Brick, Set[Brick]](settled.map(brick => {
        brick -> Set.empty[Brick]
    }).toList:_*)

    val nBricks = bricks.length

    while (!notSettled.isEmpty) {
        val unobstructed = notSettled.minBy(b => {
            b.begin.z min b.end.z
        })
        notSettled.remove(unobstructed)

        val initialMaxDrop = (unobstructed.begin.z min unobstructed.end.z) - 1
        val emptyBricks: List[Brick] = Nil
        val (dropHeight, supportedBy): (Int, List[Brick]) =
            unobstructed.foldLeft((initialMaxDrop, emptyBricks))((acc, cube) => {
                val maxDrop = acc._1
                val currentSupported = acc._2

                val (fh, supportingBrick) = floorHeight((cube.x, cube.y))
                val cubeDrop = cube.z - fh - 1
                val newMaxDrop = maxDrop min cubeDrop

                val newCurrentSupported = 
                    if (cubeDrop < maxDrop) supportingBrick.toList
                    else if (cubeDrop == maxDrop) supportingBrick.toList ::: currentSupported
                    else currentSupported


                (newMaxDrop, newCurrentSupported)
            })

        
        val droppedBrick = unobstructed.dropBy(dropHeight)
        for { cube <- droppedBrick } {
            floorHeight.updateWith((cube.x, cube.y))(height => {
                height.map(p => {
                    val (prevHeight, prevBrick) = p
                    if (cube.z >  prevHeight) (cube.z, Some(droppedBrick))
                    else p
                }).orElse(Some((cube.z, Some(droppedBrick))))
            })
        }
        settled.add(droppedBrick)
        brickSupportedBy.addOne((droppedBrick, supportedBy.toSet))
    }

    brickSupportedBy.toMap
}


def nWouldFall(removing: Brick, supports: Map[Brick, Set[Brick]], supportedBy: Map[Brick, Set[Brick]]): Int = {
    def _nWouldFall(fallen: Set[Brick], toFall: List[Brick]): Int = {
        if (toFall.isEmpty) fallen.size - 1
        else {
            val falling = toFall.head
            val newFallen = fallen + falling
            val newToFall = supports(falling).filter(supported => {
                if ((supportedBy(supported) -- newFallen).isEmpty) {
                    true
                } else false
            }).toList ::: toFall.tail

            _nWouldFall(newFallen, newToFall)
        }
    }
    _nWouldFall(Set.empty[Brick], List(removing))
}

def parse(lines: Iterator[String]): List[Brick] = lines.map(Brick.parse).toList

val bricks = parse(io.Source.stdin.getLines())
val supportedBy = settle(bricks)

val supports = (for { thisBrick <- supportedBy.keys } yield {
        val bricksSupportedByThis = (for { (otherBrick, otherSupportedBy) <- supportedBy }
        yield {
            if (otherSupportedBy.contains(thisBrick)) Some(otherBrick)
            else None
        }).flatten.toSet

        thisBrick -> bricksSupportedByThis
    }).toMap

val result = supports.keys.toList.map(brick => nWouldFall(brick, supports, supportedBy)).sum

println(result)