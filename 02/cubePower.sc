case class BagContents(val red: Int, val green: Int, val blue: Int) {
    override def toString(): String = {
        s"(${red} R, ${green} G, ${blue} B)"
    }

    def |(other: BagContents): BagContents = {
        new BagContents(
            if (red > other.red) red else other.red,
            if (green > other.green) green else other.green,
            if (blue > other.blue) blue else other.blue
        )
    }

    def power(): Int = red * green * blue
}

object BagContents {
    def nullBag(): BagContents = new BagContents(0, 0, 0)
}

sealed trait CubeColor
case object Red extends CubeColor
case object Green extends CubeColor
case object Blue extends CubeColor

def parseColor(s: String): CubeColor = {
    s match {
        case "red" => Red
        case "green" => Green
        case "blue" => Blue
        case _ => throw new RuntimeException(s"Invalid color name '${s}'")
    }
}

case class Game(val index: Int, extractions: Array[BagContents]) {
    override def toString(): String = {
        s"${index}: ${extractions.mkString(", ")}"
    }
}

def parseLine(line: String): Game = {
    assert(line.startsWith("Game "))
    val gameIndex = line.substring(5).takeWhile(_ != ':').toInt

    def parseGame(game: String): BagContents = {
        def parseElement(e: String): (CubeColor, Int) = {
            val p = e.split(" ")
            (parseColor(p(1)), p(0).toInt)
        }

        def combineElements(es: Array[(CubeColor, Int)]): BagContents = {
            var r = 0
            var g = 0
            var b = 0

            for (e <- es) {
                e._1 match {
                    case Red => r = e._2
                    case Green => g = e._2
                    case Blue => b = e._2
                }
            }

            new BagContents(r, g, b)
        }

        combineElements(game.split(", ").map(parseElement))
    }

    val games = line.dropWhile(_ != ':').substring(2).split("; ").map(parseGame)

    new Game(gameIndex, games)
}

println(io.Source.stdin.getLines().map(parseLine)
    .map(game => {
        game.extractions.foldLeft(BagContents.nullBag())((acc, b) => acc | b)
    }).map(_.power()).sum
)