'''
An alternate solution to day 24 (part two) using linear programming to check
each candidate trajectory.

This is technically correct, but it takes too long for the problem input.
'''

import numpy as np
from fractions import Fraction
from itertools import product
from scipy.optimize import linprog
from tqdm import tqdm
from sympy import Symbol, Eq, solve
from typing import Optional, Iterable

Hail = tuple[tuple[int, int, int], tuple[int, int, int]]

def main():
    hails: list[Hail] = list()
    while True:
        try:
            line = input()
            pos_str, vel_str = line.split("@")
            pos = tuple(map(lambda s: int(s.strip()), pos_str.split(",")))
            vel = tuple(map(lambda s: int(s.strip()), vel_str.split(",")))
            hails.append((pos, vel))
        except EOFError:
            break

    (x01, y01, z01), (vx1, vy1, vz1) = hails[0]
    (x02, y02, z02), (vx2, vy2, vz2) = hails[1]

    s = {n: Symbol(n) for n in [
        'x01', 'vx1', 'x02', 'vx2',
        'y01', 'vy1', 'y02', 'vy2',
        't1', 't2', 'vx', 'vy'
    ]}

    equations = [
        Eq((s['x01'] + s['vx1'] * s['t1']) - (s['x02'] + s['vx2'] * s['t2']), (s['t1'] - s['t2']) * s['vx']),
        Eq((s['y01'] + s['vy1'] * s['t1']) - (s['y02'] + s['vy2'] * s['t2']), (s['t1'] - s['t2']) * s['vy']),
    ]

    solutions = solve(equations, [s['t1'], s['t2']])
    
    def compute_t1_t2(vx: int, vy: int) -> Optional[tuple[Fraction, Fraction]]:
        t1_val = solutions[s['t1']].subs([
            (s['x01'], x01),
            (s['vx1'], vx1),
            (s['x02'], x02),
            (s['vx2'], vx2),

            (s['y01'], y01),
            (s['vy1'], vy1),
            (s['y02'], y02),
            (s['vy2'], vy2),

            (s['vx'], vx),
            (s['vy'], vy),
        ])

        t2_val = solutions[s['t2']].subs([
            (s['x01'], x01),
            (s['vx1'], vx1),
            (s['x02'], x02),
            (s['vx2'], vx2),

            (s['y01'], y01),
            (s['vy1'], vy1),
            (s['y02'], y02),
            (s['vy2'], vy2),

            (s['vx'], vx),
            (s['vy'], vy),
        ])

        if (not t1_val.is_rational) or (not t2_val.is_rational):
            return None

        if (t1_val < 0) or (t2_val < 0):
            return None

        return (
            Fraction(t1_val.numerator, t1_val.denominator),
            Fraction(t2_val.numerator, t2_val.denominator),
        )
    
    def compute_vz(t1: Fraction, t2: Fraction) -> Fraction:
        num = (z01 + vz1 * t1) - (z02 + vz2 * t2)
        den = t1 - t2
        return num / den
    
    def compute_x0(t1: Fraction, vx: int) -> Fraction:
        return x01 + (vx1 - vx) * t1
    
    def compute_y0(t1: Fraction, vy: int) -> Fraction:
        return y01 + (vy1 - vy) * t1
    
    def compute_z0(t1: Fraction, vz: int) -> Fraction:
        return z01 + (vz1 - vz) * t1
    
    def spiral() -> Iterable[tuple[int, int]]:
        di = 1
        dj = 0
        segment_length = 1
        i = 0
        j = 0
        segment_passed = 0

        yield (i, j)

        while True:
            i += di
            j += dj
            segment_passed += 1
            yield (i, j)

            if segment_passed == segment_length:
                segment_passed = 0
                di, dj = -dj, di

                if dj == 0:
                    segment_length += 1
    

    A_eq = np.zeros( (3*len(hails[2:]), len(hails[2:])), dtype=np.float64)
    b_eq = np.zeros( (3*len(hails[2:]),), dtype=np.float64)
    c = np.ones((len(hails[2:]),), dtype=np.float64)
    bounds = (0, None)
    
    for vx, vy in spiral():
        if (result := compute_t1_t2(vx, vy)) is None:
            continue
        
        t1, t2 = result

        vz = compute_vz(t1, t2)

        if vz.denominator != 1:
            continue
        else:
            vz = vz.numerator
        x0 = compute_x0(t1, vx)
        y0 = compute_y0(t1, vy)
        z0 = compute_z0(t1, vz)

        if (x0.denominator != 1) or (y0.denominator != 1) or (z0.denominator != 1):
            continue

        for idx, ((x0i, y0i, z0i), (vxi, vyi, vzi))  in enumerate(hails[2:]):
            A_eq[3*idx:3*idx+3, idx] = [float(vx - vxi), float(vy - vyi), float(vz - vzi)]
            b_eq[3*idx:3*idx+3] = [float(x0i - x0), float(y0i - y0),float(z0i - z0)]
        
        result = linprog(c, A_eq=A_eq, b_eq=b_eq, bounds=bounds)

        if result.success:
            print("solution found!")
            print(f"x0={x0.numerator}, y0={y0.numerator}, z0={z0.numerator}")
            print(f"vx={vx}, vy={vy}, z0={vz}")
            print(result.x)

            for ti, ((x0i, y0i, z0i), (vxi, vyi, vzi)) in zip(result.x, hails[2:]):
                if not np.isclose(x0  + vx  * ti, x0i + vxi * ti):
                    print(x0 + vx * ti, x0i * vxi * ti)
                    raise RuntimeError()
                
                if not np.isclose(y0  + vy  * ti, y0i + vyi * ti):
                    print(y0 + vy * ti, y0i * vyi * ti)
                    raise RuntimeError()

                if not np.isclose(z0  + vz  * ti, z0i + vzi * ti):
                    print(z0 + vz * ti, z0i * vzi * ti)
                    raise RuntimeError()

            print(x0.numerator + y0.numerator + z0.numerator)
            break
    


if __name__ == '__main__':
    main()
