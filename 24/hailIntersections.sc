import scala.language.implicitConversions

val ZERO = BigInt(0)

class Rational(_num: BigInt, _den: BigInt) extends Ordered[Rational] {
    require(_den != 0, "denominator must be non-zero")

    private def gcd(a: BigInt, b: BigInt): BigInt = (if (b == 0) a else gcd(b, a % b)).abs

    val (num, den) = {
        val g = gcd(_num, _den)
        val sign = if ((_num < 0) == (_den < 0)) 1 else -1
        ((_num.abs / g) * sign, _den.abs / g)
    }

    def this(x: BigInt) = this(x, 1)
    def this(_num: Rational, _den: Rational) = this(_num.num * _den.den, _num.den * _den.num)

    def +(other: Rational): Rational = 
        new Rational(num * other.den + other.num * den, den * other.den)

    def unary_- : Rational = new Rational(-num, den)
    def -(other: Rational) =
        new Rational(num * other.den - other.num * den, den * other.den)

    def *(other: Rational) = new Rational(num * other.num, den * other.den)
    def /(other: Rational) = new Rational(num * other.den, den * other.num)

    override def compare(that: Rational): Int =
        (num * that.den) compare (den * that.num)

    def ==(other: Rational): Boolean = ((this compare other) == 0)
    
    override def toString(): String = s"$num/$den"
}

implicit def bigIntToRational(x: BigInt): Rational = new Rational(x)

class HailTrajectory(val initialPos: (BigInt, BigInt), val velocity: (BigInt, BigInt)) {
    val (m, b): (Rational, Rational) = {
        val (x0, y0) = initialPos
        val (vx, vy) = velocity
        val _m = new Rational(vy, vx)
        (_m, y0 - _m * x0)
    }

    def intersects(other: HailTrajectory): Option[(Rational, Rational)] = {
        val (m1, b1) = (m, b)
        val (m2, b2) = (other.m, other.b)

        if (m1 == m2) {
            if ((b1 == b2) && (initialPos == other.initialPos)) Some((initialPos._1, initialPos._2))
            else None
        }
        else {
            val ((x01, _), (vx1, _)) = (initialPos, velocity)
            val ((x02, _), (vx2, _))= (other.initialPos, other.velocity)
            val x = new Rational(b2 - b1, m1 - m2)
            val y = m1 * x + b1
            
            if (
                (((x - x01) < ZERO) == (vx1 < ZERO)) &&
                (((x - x02) < ZERO) == (vx2 < ZERO))
            ) Some((x, y))
            else None
        }
    }

    override def toString(): String = s"${initialPos._1}, ${initialPos._2}, ${initialPos._2} @ ${velocity._1}, ${velocity._2}, ${velocity._2}"
}

object HailTrajectory {
    def parse(line: String): HailTrajectory = {
        val lineParts = line.split("\\s*@\\s*")
        val posString = lineParts(0)
        val velString = lineParts(1)

        val posList = posString.split(",\\s*").map(s => BigInt(s))
        val initialPos = (posList(0), posList(1))

        val velList = velString.split(",\\s*").map(s => BigInt(s))
        val velocity = (velList(0), velList(1))

        new HailTrajectory(initialPos, velocity)
    }
}

def nIntersections(trajectories: List[HailTrajectory], xBounds: (BigInt, BigInt), yBounds: (BigInt, BigInt)): Int = {
    val (xMin, xMax) = xBounds
    val (yMin, yMax) = yBounds

    def _nIntersections(acc: Int, current: HailTrajectory, others: List[HailTrajectory]): Int = {
        if (others.isEmpty) acc
        else {
            val newIntersections = others.count(other => {
                (current intersects other).map({case (xi, yi) =>
                    (xMin <= xi) && (xi <= xMax) && (yMin <= yi) && (yi <= yMax)
                }).getOrElse(false)
            })
            _nIntersections(acc + newIntersections, others.head, others.tail)
        }
    }

    _nIntersections(0, trajectories.head, trajectories.tail)
}

val trajectories = io.Source.stdin.getLines().map(HailTrajectory.parse).toList
val xBounds = (BigInt("200000000000000"), BigInt("400000000000000"))
val yBounds = xBounds
val result = nIntersections(trajectories, xBounds, yBounds)
println(result)