sealed trait Direction {
    val movement: (Int, Int)
    def move(pos: (Int, Int)): (Int, Int) =
        (pos._1 + movement._1, pos._2 + movement._2)
}
object Left extends Direction {
    override val movement: (Int, Int) = (0, -1)
    override def toString(): String = "Left"
}
object Right extends Direction{
    override val movement: (Int, Int) = (0, 1)
    override def toString(): String = "Right"
}
object Up extends Direction{
    override val movement: (Int, Int) = (-1, 0)
    override def toString(): String = "Up"
}
object Down extends Direction{
    override val movement: (Int, Int) = (1, 0)
    override def toString(): String = "Down"
}

object Direction{
    val allDirections: List[Direction] = List(Left, Right, Up, Down)
}

case class Node(pos: (Int, Int), connections: List[Direction]) {
    val neighbours: List[(Int, Int)] = connections.map(_.move(pos))
}

sealed trait Tile
object EmptyTile extends Tile
case class SlopedTile(direction: Direction) extends Tile
object WallTile extends Tile

object Tile {
    def parse(c: Char): Tile = {
        c match {
            case '.' => EmptyTile
            case '#' => WallTile
            case '<' => SlopedTile(Left)
            case '>' => SlopedTile(Right)
            case '^' => SlopedTile(Up)
            case 'v' => SlopedTile(Down)
            case _ => sys.error("Invalid character")
        }
    }
}

type Graph = Map[(Int, Int), List[(Int, Int)]]

def parse(lines: Iterator[String]): Array[Array[Tile]] = {
    lines.map(
        line => line.map(Tile.parse).toArray
    ).toArray ensuring(lines => lines.forall(line => line.length == lines(0).length))
}

def buildGraph(tiles: Array[Array[Tile]]): Graph = {
    val nRows = tiles.length
    val nCols = tiles(0).length

    def insideMap(pos: (Int, Int)) =
        (pos._1 >= 0) && (pos._1 < nRows) && (pos._2 >= 0) && (pos._2 < nCols)

    (for {
        (row, i) <- tiles.zipWithIndex
        (tile, j) <- row.zipWithIndex
    } yield {
        tile match {
            case WallTile => None
            case SlopedTile(direction) => {
                val neighbours = List(direction).map(_.move((i, j)))
                    .filter(pn => {
                        val (ni, nj) = pn
                        insideMap(pn) && (tiles(ni)(nj) != WallTile)
                    })
                Some((i, j) -> neighbours)
            }
            case EmptyTile => {
                val neighbours = Direction.allDirections.map(_.move((i, j)))
                    .filter(pn => {
                        val (ni, nj) = pn
                        insideMap(pn) && (tiles(ni)(nj) != WallTile)
                    })
                Some((i, j) -> neighbours)
            }
        }
    }).flatten.toMap
}

def buildDAG(graph: Graph, root: (Int, Int)): Graph = {
    // Use LinkedHashList so that the frontier is explored in
    // insertion order without repeats.
    // Keeping track of the previous node is enough to not fall into
    // any cycles, as the paths are always 1 tile wide.
    // This allows the process to find diverging and converging paths.
    val frontier = collection.mutable.LinkedHashSet[((Int, Int), (Int, Int))]()

    def _buildDAG(currentGraph: Graph): Graph = {
        if (frontier.isEmpty) currentGraph
        else {
            val h @ (node, cameFrom) = frontier.head
            val neighbours = graph(node).filter(n => n != cameFrom)
            frontier.remove(h)
            frontier.addAll(neighbours.map((_, node)))

            val newcurrentGraph = currentGraph.updated(node, neighbours)
            _buildDAG(newcurrentGraph)
        }
    }

    frontier.addOne((root, root))
    _buildDAG(Map.empty[(Int, Int), List[(Int, Int)]])
}

def longestPathLength(dag: Graph, source: (Int, Int), destination: (Int, Int)): Int = {
    // Finding the longest path in a DAG is the same as finding the shortest path,
    // but transforming all edge weights to their negative. In our case, all edges
    // have a weight of 1, so the weight of every edge will now be -1. Then, just
    // run Dijkstra's algorithm

    def _longestPathLength(dist: Map[(Int, Int), Option[Int]], unexplored: Set[(Int, Int)]): Int = {
        if (unexplored.isEmpty) sys.error("Could not reach destination")
        else {
            val node = unexplored.minBy(p => dist(p).getOrElse(Int.MaxValue))
            val nodeDist = dist(node).get

            if (node == destination) nodeDist.abs
            else {
                val newDist = dag(node)
                    .filter(unexplored.contains(_))
                    .foldLeft(dist)((acc, neighbour) => {
                        val currentDist = acc(neighbour)
                        val newDist = nodeDist - 1

                        if (currentDist.map(newDist < _).getOrElse(true))
                            acc.updated(neighbour, Some(newDist))
                        else acc
                    })
                
                _longestPathLength(newDist, unexplored - node)
            }
        }
    }

    _longestPathLength(
        Map[(Int, Int), Option[Int]](source -> Some(0)).withDefault(_ => None),
        dag.keySet
    )
}

def graphviz(graph: Graph, directed: Boolean = true): String = {
    val edge = if (directed) "->" else "--"
    val tab = "    "
    val graphTitle = if (directed) "digraph G" else "graph G"

    graphTitle + "{\n" +
    graph.map({ case (node, children) =>
        def name(n: (Int, Int)): String = s"r${n._1}c${n._2}"

        tab + (
            if (children.isEmpty) s"${name(node)};"
            else s"${name(node)} ${edge} ${children.map(name)mkString(", ")};"
        )
    }).mkString("\n") + "\n}"
}

val tiles = parse(io.Source.stdin.getLines())

val initialCol = tiles(0).indexWhere(_ != WallTile)
val initialPos = (0, initialCol)

val targetCol = tiles(tiles.length - 1).indexWhere(_ != WallTile)
val targetPos = (tiles.length - 1, targetCol)

val graph = buildGraph(tiles)
val dag = buildDAG(graph, initialPos)
//println(graphviz(dag, directed = true))

val result = longestPathLength(dag, initialPos, targetPos)
println(result)