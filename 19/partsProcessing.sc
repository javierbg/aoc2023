/*
Grammar:

<program> ::= <list-workflows>
<list-workflows> ::= <workflow> <list-workflows> | 0
<workflow> ::= <id> '{' <rule-list> '}'
<rule-list> ::= <rule> ',' <rule-list> | <default-rule> 
<rule> ::= <condition> ':' <action>
<condition> ::= <id> '<' <number> | <id> '>' <number>
<default-rule> ::= <action>
<action> ::= <id>

Tokens:
<id>
'<' '>'
':'
'{' '}'
<number>
','
*/

sealed trait Token {
    def isIdentifier: Boolean = false
    def isNumber: Boolean = false
}
object LessThan extends Token
object GreaterThan extends Token
object Colon extends Token
object OpenBrace extends Token
object CloseBrace extends Token
object Comma extends Token
case class Identifier(id: String) extends Token {
    override def isIdentifier: Boolean = true
}
case class Number(n: Int) extends Token {
    override def isNumber: Boolean = true
}

object AnyIdentifier extends Identifier("")
object AnyNumber extends Number(0)

def tokenize(text: String): List[Token] = {
    def _tokenize(acc: List[Token], remaining: String): List[Token] = {
        if (remaining.isEmpty) acc.reverse
        else {
            val trimmedRemaining = remaining.trim()
            val (nextToken, nConsumed) = trimmedRemaining.charAt(0) match {
                case '<' => (LessThan, 1)
                case '>' => (GreaterThan, 1)
                case ':' => (Colon, 1)
                case '{' => (OpenBrace, 1)
                case '}' => (CloseBrace, 1)
                case ',' => (Comma, 1)
                case c if c.isDigit => {
                    val numberString = trimmedRemaining.takeWhile(_.isDigit)
                    (new Number(numberString.toInt), numberString.length)
                }
                case c if c.isLetter => {
                    val idString = trimmedRemaining.takeWhile(_.isLetter)
                    (new Identifier(idString), idString.length)
                }
                case c => sys.error(s"Invalid next character: '$c'")
            }

            _tokenize(nextToken :: acc, trimmedRemaining.drop(nConsumed))
        }
    }
    _tokenize(Nil, text)
}

case class Part(x: Int, m: Int, a: Int, s: Int) {
    def rating: Int = x + m + a + s
}

object Part {
    def parse(line: String): Part = {
        assert(line(0) == '{')
        assert(line(line.length()-1) == '}')
        val trimmedLine = line.substring(1, line.length()-1)
        val splitLine = trimmedLine.split(",")
        val fieldMap = (for {part <- splitLine} yield {
            val ps = part.split("=")
            val fieldName = ps(0)
            val fieldValue = ps(1).toInt
            fieldName -> fieldValue
        }).toMap

        new Part(fieldMap("x"), fieldMap("m"), fieldMap("a"), fieldMap("s"))
    }
}

case class Condition(condition: Part => Boolean, repr: String) {
    def apply(part: Part) = condition(part)
    override def toString(): String = repr
}

sealed trait Action
object Accept extends Action
object Reject extends Action
case class Goto(workflowName: String) extends Action

case class Rule(condition: Condition, action: Action)
case class Workflow(rules: List[Rule])
case class Program(workflows: Map[String, Workflow]) {
    def accept(part: Part): Boolean = {
        def _accept(remainingRules: List[Rule]): Boolean = {
            if (remainingRules.isEmpty) sys.error("Ran out of rules")
            else {
                val nextRule = remainingRules.head
                if (nextRule.condition(part)) {
                    nextRule.action match {
                        case Accept => true
                        case Reject => false
                        case Goto(workflowName) => _accept(workflows(workflowName).rules)
                    }
                } else _accept(remainingRules.tail)
            }
        }

        _accept(workflows("in").rules)
    }
}

def expectSymbol(token: Token, tokens: List[Token]): Option[Token] = {
    if (tokens.isEmpty) None
    else if (token == tokens.head) Some(tokens.head)
    else None
}

def expectIdentifier(tokens: List[Token]): Option[Identifier] = {
    if (tokens.isEmpty) None
    else tokens.head match {
        case id @ Identifier(_) => Some(id)
        case _ => None
    }
}

def expectNumber(tokens: List[Token]): Option[Number] = {
    if (tokens.isEmpty) None
    else tokens.head match {
        case n @ Number(_) => Some(n)
        case _ => None
    }
}

def parseProgram(tokens: List[Token]): Option[Program] = {
    parseWorkflowList(tokens).map(p => (Program(p._1), p._2))
        .map(p => {
            if (p._2.isEmpty) Some(p._1)
            else None
        }).flatten
}

def parseWorkflowList(tokens: List[Token]): Option[(Map[String, Workflow], List[Token])] = {
    def _parseWorkflowList(acc: List[(String, Workflow)], remTokens: List[Token]): Option[(Map[String, Workflow], List[Token])] = {
        if (remTokens.isEmpty) Some((acc.toMap, remTokens))
        else {
            val wf = parseWorkflow(remTokens)

            if (wf.isDefined) {
                val (wfName, workflow, rest) = wf.get
                _parseWorkflowList((wfName, workflow) :: acc, rest)
            } else None
        }
    }

    
    _parseWorkflowList(Nil, tokens)
}

def parseWorkflow(tokens: List[Token]): Option[(String, Workflow, List[Token])] = {
    expectIdentifier(tokens)
        .map(wfid => expectSymbol(OpenBrace, tokens.drop(1)).map(_ => wfid)).flatten
        .map(wfid => parseRuleList(tokens.drop(2)).map(p => (wfid, p._1, p._2))).flatten
        .map(p => expectSymbol(CloseBrace, p._3).map(_ => (p._1, p._2, p._3.tail))).flatten
        .map(p => (p._1.id, new Workflow(p._2), p._3))
}

def parseRuleList(tokens: List[Token]): Option[(List[Rule], List[Token])] = {
    def _parseRuleList(acc: List[Rule], remTokens: List[Token]): Option[(List[Rule], List[Token])] = {
        if (remTokens.isEmpty || expectIdentifier(remTokens).isEmpty) None

        val r = parseRule(remTokens)

        if (r.isDefined) {
            val (rule, rest) = r.get
            if (expectSymbol(Comma, rest).isDefined)
                _parseRuleList(rule :: acc, rest.drop(1))
            else None
        } else {
            parseDefaultRule(remTokens)
                .map(p => ((p._1 :: acc).reverse, p._2))
        }
    }

    
    _parseRuleList(Nil, tokens)
}

def parseRule(tokens: List[Token]): Option[(Rule, List[Token])] = {
    parseCondition(tokens)
        .map(p => expectSymbol(Colon, p._2).map(_ => (p._1, p._2.tail))).flatten
        .map(p => parseAction(p._2).map(p1 => (p._1, p1._1, p1._2))).flatten
        .map(p => {
            val (condition, action, rest) = p
            (new Rule(condition, action), rest)
        })
}

def parseAction(tokens: List[Token]): Option[(Action, List[Token])] = {
    expectIdentifier(tokens).map(id => {
        val action: Action = id.id match {
            case "R" => Reject
            case "A" => Accept
            case s => Goto(s)
        }

        (action, tokens.tail)
    })
}

def parseDefaultRule(tokens: List[Token]): Option[(Rule, List[Token])] = {
    parseAction(tokens).map(p => {
        val (action, rest) = p

        (new Rule(new Condition(_ => true, ""), action), rest)
    })
}

def parseCondition(tokens: List[Token]): Option[(Condition, List[Token])] = {
    expectIdentifier(tokens)
        .zip(expectSymbol(LessThan, tokens.drop(1)))
        .zip(expectNumber(tokens.drop(2)))
        .map(ts => {
            val ((id, _), n) = ts
            (new Condition(part => id.id match {
                case "x" => part.x < n.n
                case "m" => part.m < n.n
                case "a" => part.a < n.n
                case "s" => part.s < n.n
                case _ => sys.error(s"Condition on invalid field '${id.id}'")
            }, s"${id.id} < ${n.n}"), tokens.drop(3))
    }).orElse(expectIdentifier(tokens)
        .zip(expectSymbol(GreaterThan, tokens.drop(1)))
        .zip(expectNumber(tokens.drop(2)))
        .map(ts => {
            val ((id, _), n) = ts
            (new Condition(part => id.id match {
                case "x" => part.x > n.n
                case "m" => part.m > n.n
                case "a" => part.a > n.n
                case "s" => part.s > n.n
                case _ => sys.error(s"Condition on invalid field '${id.id}'")
            }, s"${id.id} > ${n.n}"), tokens.drop(3))
    }))
}

def parse(lines: Iterator[String]): (Program, List[Part]) = {
    val tokens = tokenize(lines.takeWhile(_ != "").mkString("\n"))
    val program = parseProgram(tokens)
    
    val partsList = lines.map(Part.parse).toList

    program.map(p => (p, partsList)).getOrElse(sys.error("Invalid program"))
}

val (program, parts) = parse(io.Source.stdin.getLines())
val result = parts.filter(part => program.accept(part)).map(_.rating).sum
println(result)