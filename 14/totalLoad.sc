sealed trait Space
case object Empty extends Space
case object StillRock extends Space
case object RollingRock extends Space

class Board(cols: Array[Array[Space]]) {
    def northLoad: Int = {
        def colNorthLoad(colIdx: Int) = {
            val col = cols(colIdx)

            def _colNorthLoad(acc: Int, lastStillRow: Int, rowIdx: Int): Int = {
                if (rowIdx >= col.length) acc
                else col(rowIdx) match {
                    case Empty => _colNorthLoad(acc, lastStillRow, rowIdx + 1)
                    case StillRock => _colNorthLoad(acc, rowIdx, rowIdx + 1)
                    case RollingRock => _colNorthLoad(
                        acc + (col.length - lastStillRow - 1),
                        lastStillRow + 1,
                        rowIdx + 1
                    )
                }
            }
            _colNorthLoad(0, -1, 0)
        }
        (0 until cols.length).map(colNorthLoad).sum
    }
}

def parse(lines: Iterator[String]): Board = {
    val cols: Array[Array[Space]] = lines.map(line => {
        line.map(c => c match {
            case 'O' => RollingRock
            case '#' => StillRock
            case '.' => Empty
            case _ => sys.error("Invalid character")
        }).toArray[Space]
    }).toArray.transpose

    new Board(cols)
}

val board = parse(io.Source.stdin.getLines())
val result = board.northLoad
println(result)
