sealed trait Direction {
    val movement: (Long, Long)
    def move(pos: (Long, Long), nSteps: Long = 1): (Long, Long) =
        (pos._1 + movement._1*nSteps, pos._2 + movement._2*nSteps)
}
object Left extends Direction {
    override val movement: (Long, Long) = (0, -1)
    override def toString(): String = "Left"
}
object Right extends Direction{
    override val movement: (Long, Long) = (0, 1)
    override def toString(): String = "Right"
}
object Up extends Direction{
    override val movement: (Long, Long) = (-1, 0)
    override def toString(): String = "Up"
}
object Down extends Direction{
    override val movement: (Long, Long) = (1, 0)
    override def toString(): String = "Down"
}

object Direction {
    def parse(c: Char): Direction = c match {
        case '0' => Right
        case '1' => Down
        case '2' => Left
        case '3' => Up
        case _ => sys.error("Invalid character")
    }
}

case class Move(direction: Direction, distance: Long)

case class DigPlan(moves: List[Move])

object DigPlan {
    def parse(lines: Iterator[String]): DigPlan = {
        new DigPlan(lines.map(line => {
            val parts = line.split("\\s+")
            
            val hexString = parts(2).drop(2).dropRight(1)
            assert(hexString.length() == 6)
            val distance = java.lang.Long.parseLong(hexString.take(5), 16)
            val direction = Direction.parse(hexString.charAt(5))

            new Move(direction, distance)
        }).toList)
    }
}

def lagoonSize(plan: DigPlan): Long = {
    // Shoelace formula
    def computeArea(acc: Long, currentPos: (Long, Long), remainingMoves: List[Move]): Long = {
        if (remainingMoves.isEmpty) acc.abs / 2
        else {
            val nextMove = remainingMoves.head
            val nextPos = nextMove.direction.move(currentPos, nextMove.distance)
            computeArea(
                acc + ((currentPos._1 * nextPos._2) - (currentPos._2 * nextPos._1)),
                nextPos,
                remainingMoves.tail
            )
        }
    }

    val area = computeArea(0L, (0L, 0L), plan.moves)
    val nBoundaryPoints = plan.moves.map(move => move.distance).sum
    assert((nBoundaryPoints % 2L) == 0)
    val nInteriorPoints = area - (nBoundaryPoints / 2L) + 1L // by Pick's theorem

    nBoundaryPoints + nInteriorPoints
}

val plan = DigPlan.parse(io.Source.stdin.getLines())
val result = lagoonSize(plan)
println(result)