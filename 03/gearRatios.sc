sealed trait EnginePart {
    override def toString(): String = {
        this match {
            case Empty => "."
            case Digit(value) => value.toString
            case Part(value) => value.toString
        }
    }

    def isDigit: Boolean = {
        this match {
            case Digit(value) => true
            case _ => false
        }
    }

    def digitValue: Option[Int] = {
        this match {
            case Digit(value) => Some(value)
            case _ => None 
        }
    }

    def isPart: Boolean = {
        this match {
            case Part(value) => true
            case _ => false
        }
    }

    def isGear: Boolean = {
        this match {
            case Part('*') => true
            case _ => false
        }
    }
}

case object Empty extends EnginePart
case class Digit(value: Int) extends EnginePart
case class Part(value: Char) extends EnginePart

object EnginePart {
    def parse(c: Char): EnginePart = {
        c match {
            case '.' => Empty
            case x if x.isDigit => Digit(x.toInt - 48)
            case _ => Part(c)
        }
    }
}

class EngineSchematic(matrix: Array[Array[EnginePart]]) extends Iterable[Array[EnginePart]] {
    require(matrix.map(line => line.length == matrix(0).length).forall(identity))
    
    private val m = matrix

    override def toString(): String = {
        m.map(line => {
            line.map(_.toString).mkString("")
        }).mkString("\n")
    }

    def nRows(): Int = m.length
    def nCols(): Int = if (m.length == 0) 0 else m(0).length

    def apply(i: Int): Array[EnginePart] = {
        m(i)
    }

    override def iterator: Iterator[Array[EnginePart]] = m.iterator
}

def parseInput(lines: Iterator[String]): EngineSchematic = {
    new EngineSchematic(
        (for (line <- lines) yield line.map(EnginePart.parse).toArray).toArray
    )
}

case class PartNumber(number: Int, row: Int, startCol: Int, endCol: Int) {
    def gearsTouchingNumber(sch: EngineSchematic): List[(Int, Int)] = {
        def addGearsInArray(remaining: Array[EnginePart], offset: Int, currentRow: Int, accumulated: List[(Int, Int)]): List[(Int, Int)] = {
            if (remaining.isEmpty) accumulated
            else {
                val newAccumulated = if (remaining(0).isGear) ((currentRow, offset) :: accumulated) else accumulated
                addGearsInArray(remaining.drop(1), offset + 1, currentRow, newAccumulated)
            }
        }

        val gears1 = if (row > 0)               addGearsInArray(sch(row-1).slice(startCol, endCol), startCol, row-1, Nil)    else Nil
        val gears2 = if (row < (sch.nRows()-1)) addGearsInArray(sch(row+1).slice(startCol, endCol), startCol, row+1, gears1) else gears1
        val gears3 = if ((row > 0)               && (startCol > 0)         && (sch(row-1)(startCol-1).isGear)) (row-1, startCol-1) :: gears2 else gears2
        val gears4 = if ((row < (sch.nRows()-1)) && (startCol > 0)         && (sch(row+1)(startCol-1).isGear)) (row+1, startCol-1) :: gears3 else gears3
        val gears5 = if ((row > 0)               && (endCol < sch.nCols()) && (sch(row-1)(endCol).isGear))     (row-1, endCol) :: gears4 else gears4
        val gears6 = if ((row < (sch.nRows()-1)) && (endCol < sch.nCols()) && (sch(row+1)(endCol).isGear))     (row+1, endCol) :: gears5 else gears5
        val gears7 = if ((startCol > 0)         && sch(row)(startCol-1).isGear) (row, startCol-1) :: gears6 else gears6
        val gears8 = if ((endCol < sch.nCols()) && sch(row)(endCol).isGear)     (row, endCol    ) :: gears7     else gears7

        gears8
    }
}

def findAllNumbers(engineSchematic: EngineSchematic): List[PartNumber] = {
    def digitsToNumber(digits: Array[Int]): Int = {
        digits.foldLeft(0)((acc, next) => 10*acc + next)
    }

    def getNumbersInRow(remaining: Array[EnginePart], offset: Int, row: Int, accumulated: List[PartNumber]): List[PartNumber] = {
        val nSkipped = remaining.takeWhile(!_.isDigit).length
        val possibleNext = remaining.dropWhile(!_.isDigit)
        
        if (possibleNext.isEmpty)
            accumulated 
        else {
            val digitRun = possibleNext.takeWhile(_.isDigit)
            val nextNumber: Int = digitsToNumber(digitRun.map(_.digitValue.get))
            val newAccumulated = (new PartNumber(
                    nextNumber,
                    row,
                    offset + nSkipped,
                    offset + nSkipped + digitRun.length
                )) :: accumulated
            getNumbersInRow(possibleNext.dropWhile(_.isDigit), offset + nSkipped + digitRun.length, row, newAccumulated)
        }
    }

    def getNumbersInSchematic(it: Iterator[Array[EnginePart]], accumulated: List[PartNumber], nextRow: Int): List[PartNumber] = {
        if (it.hasNext) {
            val withFirstRow = getNumbersInRow(it.next(), 0, nextRow, accumulated)
            getNumbersInSchematic(it, withFirstRow, nextRow + 1)
        } else
            accumulated
    }

    getNumbersInSchematic(engineSchematic.iterator, Nil, 0)
}


val schematic = parseInput(io.Source.stdin.getLines())
val numbers = findAllNumbers(schematic)
val gearsTouchingNumbers = numbers.map(_.gearsTouchingNumber(schematic))
val uniqueGearsTouchingNumbers = gearsTouchingNumbers.flatten(identity).toSet
val result = uniqueGearsTouchingNumbers.foldLeft(0)((acc, gear) => {
    val numbersTouchingGear = numbers.zip(gearsTouchingNumbers).filter(pair => pair._2.contains(gear)).map(_._1)
    numbersTouchingGear match {
        case n1 :: n2 :: Nil => acc + (n1.number * n2.number)
        case _ => acc
    }
})
println(result)