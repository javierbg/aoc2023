sealed trait Space
object Empty extends Space {
    override def toString(): String = "."
}
case class Mirror(forward: Boolean) extends Space {
    def newDirection(inputDirection: Direction): Direction = {
        val newDirection = inputDirection match {
            case Left => Down
            case Right => Up
            case Up => Right
            case Down => Left
        }
        if (forward) newDirection
        else newDirection.inverse
    }

    override def toString(): String = if (forward) "/" else "\\"
}
case class Splitter(horizontal: Boolean) extends Space {
    def newDirections(inputDirection: Direction): Option[List[Direction]] = {
        if (horizontal == inputDirection.horizontal) None
        else 
            if (horizontal) Some(List(Left, Right))
            else Some(List(Up, Down))
    }

    override def toString(): String = if (horizontal) "-" else "|"
}
object Space {
    def parse(c: Char): Space = c match {
        case '.' => Empty
        case '/' => new Mirror(true)
        case '\\' => new Mirror(false)
        case '-' => new Splitter(true)
        case '|' => new Splitter(false)
        case _ => sys.error("Invalid character")
    }
}

sealed trait Direction {
    val movement: (Int, Int)
    def inverse: Direction
    val horizontal: Boolean
    def move(pos: (Int, Int)): (Int, Int) = (pos._1 + movement._1, pos._2 + movement._2)
}
object Left extends Direction {
    override val movement: (Int, Int) = (0, -1)
    override def inverse: Direction = Right
    override val horizontal: Boolean = true
}
object Right extends Direction{
    override val movement: (Int, Int) = (0, 1)
    override def inverse: Direction = Left
    override val horizontal: Boolean = true
}
object Up extends Direction{
    override val movement: (Int, Int) = (-1, 0)
    override def inverse: Direction = Down
    override val horizontal: Boolean = false
}
object Down extends Direction{
    override val movement: (Int, Int) = (1, 0)
    override def inverse: Direction = Up
    override val horizontal: Boolean = false
}

class Board(b: Array[Array[Space]]) {
    require(b.forall(row => row.length == b(0).length))

    val board = b

    val nRows = b.length
    val nCols = b(0).length

    def positionInBoard(pos: (Int, Int)): Boolean = {
        val (r, c) = pos
        (r >= 0) && (r < nRows) && (c >= 0) && (c < nCols)
    }

    def energizedByBeam: Int = {
        val energized: Array[Array[Boolean]] =
            (for {
                i <- 0 until nRows
            } yield (for {
                j <- 0 until nCols
            } yield false).toArray).toArray

        val history = collection.mutable.HashSet[((Int, Int), Direction)]()

        def moveBeam(currentPosition: (Int, Int), direction: Direction): Unit = {
            if (!positionInBoard(currentPosition)) ()
            else if (history.contains((currentPosition, direction))) ()
            else {
                history.add((currentPosition, direction))
                val (r, c) = currentPosition
                energized(r)(c) = true
                val currentSpace = board(r)(c)
                currentSpace match {
                    case Empty => moveBeam(direction.move(currentPosition), direction)
                    case mirror @ Mirror(forward) => {
                        val newDirection = mirror.newDirection(direction)
                        moveBeam(newDirection.move(currentPosition), newDirection)
                    }
                    case splitter @ Splitter(horizontal) => {
                        val possibleNewDirections = splitter.newDirections(direction)
                        if (possibleNewDirections.isDefined)
                            possibleNewDirections.foreach(newDirections => {
                                newDirections.foreach(newDirection => {
                                    moveBeam(newDirection.move(currentPosition), newDirection)
                                })
                            })
                        else moveBeam(direction.move(currentPosition), direction)
                    }
                }
            }
        }

        val initialPos = (0, 0)
        val initialDirection = Right
        moveBeam(initialPos, initialDirection)

        energized.map(row => {
            row.map(e => if (e) 1 else 0).sum
        }).sum
    }

    override def toString(): String = board.map(row => {
        row.map(_.toString).mkString("")
    }).mkString("\n")
}

object Board {
    def parse(lines: Iterator[String]): Board = {
        new Board(
            (for {line <- lines} yield {
                line.map(Space.parse).toArray
            }).toArray
        )
    }
}

val board = Board.parse(io.Source.stdin.getLines())
val result = board.energizedByBeam
println(result)